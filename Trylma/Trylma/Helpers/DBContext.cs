﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Trylma.Models;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace Trylma.Helpers
{
    static public class DBContext
    {
        //static String server = "ALICE-PC\\ALICESQLSERVER";
        static String server = "DV6-HP\\MSSQLSERVER2012";
        static String databaseName = "TRYLMA_DATABASE";

        //private static SqlConnection connection;

        // inicjacja bazy danych - pobranie AI i ustawienie ich w kodzie
        public static void init()
        {
            //connection = new SqlConnection(@"data source=" + server + "; initial catalog=" + databaseName + "; Integrated Security=SSPI;");
            //connection.Open();

            User user = AskBase("getUserByLogin", new List<SqlParameter> { new SqlParameter("@login", "HeuristicAI") });
            User.HeuristicAIUser = user;
            user = AskBase("getUserByLogin", new List<SqlParameter> { new SqlParameter("@login", "MinMaxAI") });
            User.MinMaxAIUser = user;
        }

        // zwalnia strumienie otwierane podczas łączenia się z bazą
        private static void DisposeAll(IEnumerable collection)
        {
            foreach (var obj in collection.OfType<IDisposable>())
            {
                IDisposable disp = obj as IDisposable;
                if (disp != null)
                {
                    disp.Dispose();
                    disp = null;
                }
            }
        }

        private static string GetHash(MD5 md5Hash, string word)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(word));
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                stringBuilder.Append(data[i].ToString("x2")); // format as a hexadecimal string
            }

            return stringBuilder.ToString();
        }

        /* łączenie się z bazą - w praktyce sam select
         zwraca Usera, który jest wynikiem zapytania bazodanowego  */
        private static User AskBase(string command, List<SqlParameter> listParameter)
        {
            SqlDataReader reader = null;

            SqlConnection connection = new SqlConnection(@"data source=" + server + "; initial catalog=" + databaseName + "; Integrated Security=SSPI;");
            connection.Open();

            SqlCommand cmd = new SqlCommand(command, connection);
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter param in listParameter)
            {
                cmd.Parameters.Add(param);
            }
            reader = cmd.ExecuteReader();
            User user = null;
            while (reader.Read())
            {
                user = new User((int)reader["id"], reader["login"].ToString(), reader["password"].ToString(), Convert.ToInt32(reader["playerType"].ToString()), (int)reader["ranking"],reader["avatar"].ToString());
                // bez avatara:
               // user = new User((int)reader["id"], reader["login"].ToString(), reader["password"].ToString(),Convert.ToInt32(reader["playerType"].ToString()),(int)reader["ranking"]);
            }
            DisposeAll(new List<Object> { reader, connection });

            return user;
        }

        public static void ChangeRanking(User user)
        {
            OrderDatabase("changeRanking", new List<SqlParameter> { new SqlParameter("@login",user.Name), new SqlParameter("@ranking",user.Ranking)});
        }

        private static bool OrderDatabase(string command, List<SqlParameter> listParameter)
        {
            SqlDataReader reader = null;
            SqlConnection connection = new SqlConnection(@"data source=" + server + "; initial catalog=" + databaseName + "; Integrated Security=SSPI;");
            connection.Open();
            SqlCommand cmd = new SqlCommand(command, connection);
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (SqlParameter param in listParameter)
            {
                cmd.Parameters.Add(param);
            }
            try
            {
                reader = cmd.ExecuteReader();
                reader.Read();
                DisposeAll(new List<Object> { reader, connection });
                return true;
            }
            catch (SqlException e)
            {
                DisposeAll(new List<Object> { connection });
                return false;
            }
        }

        static public bool AddNewUserToDatabase(string login, string password, string note)
        {
            //SqlDataReader reader = null;
            //SqlConnection connection = new SqlConnection(@"data source=" + server + "; initial catalog=" + databaseName + "; Integrated Security=SSPI;");
            //connection.Open();
            //SqlCommand cmd = new SqlCommand("addNewUser", connection);
            //cmd.CommandType = CommandType.StoredProcedure;
           // cmd.Parameters.Add(new SqlParameter("@login", login));
            MD5 md5Hash = MD5.Create();
            string hashedPassword = GetHash(md5Hash, password);
            //cmd.Parameters.Add(new SqlParameter("@password", hashedPassword));
            //cmd.Parameters.Add(new SqlParameter("@opis", note));
            return OrderDatabase("addNewUser", new List<SqlParameter> { new SqlParameter("@login", login), new SqlParameter("@password", hashedPassword), new SqlParameter("@opis", note) });

            //byte[] array;
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    file.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            //    array = ms.ToArray();
            //}
            //cmd.Parameters.Add("@avatar", array);

            //try
            //{
            //    reader = cmd.ExecuteReader();
            //    reader.Read();
            //    DisposeAll(new List<Object> { reader, connection });
            //    return true;
            //}
            //catch
            //{
            //    DisposeAll(new List<Object> { connection });
            //    return false;
            //}
        }

        // sprawdza czy w bazie istnieje dany user
        static public bool UserExists(string login, string password)
        {
            MD5 md5Hash = MD5.Create();
            string hashedPassword = GetHash(md5Hash, password);  //password;
            if (AskBase("getUser", new List<SqlParameter> { new SqlParameter("@login", login), new SqlParameter("@password", hashedPassword) }) != null)
                return true;
            else
                return false;
        }

        /* pobiera z bazy usera o podanym loginie
         zwraca użytkownika lub nulla, jeśli taki nie istnieje */
        static public User GetUserByLogin(string login)
        {
            return AskBase("getUserByLogin", new List<SqlParameter> { new SqlParameter("@login", login) });
        }

        /* pobiera z bazy usera o podanym id
         zwraca uzytkownika lub nulla, jeśli taki nie istnieje */
        static public User GetUserById(int id)
        {
            return AskBase("getUserById", new List<SqlParameter> { new SqlParameter("@id", id) });
        }

        static public User SetAvatar(string login, string path)
        {
            return AskBase("setAvatar", new List<SqlParameter> { new SqlParameter("@login", login), new SqlParameter("@imgpath", path) });
        }

        public static List<User> GetRankingList()
        {
            SqlDataReader reader = null;
            SqlConnection connection = new SqlConnection(@"data source=" + server + "; initial catalog=" + databaseName + "; Integrated Security=SSPI;");
            connection.Open();
            SqlCommand cmd = new SqlCommand("getRankingList", connection);
            cmd.CommandType = CommandType.StoredProcedure;
            reader = cmd.ExecuteReader();

            List<User> users = new List<User>();
            while (reader.Read())
            {
                users.Add(new User((int)reader["id"], reader["login"].ToString(), reader["password"].ToString(), Convert.ToInt32(reader["playerType"].ToString()), (int)reader["ranking"], reader["avatar"].ToString()));
            }
            DisposeAll(new List<Object> { reader, connection });

            return users;
        }
    }
}