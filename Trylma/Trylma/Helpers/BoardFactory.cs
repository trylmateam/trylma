﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Trylma.Models;

namespace Trylma.Helpers
{
    public class BoardFactory
    {
        private string[] StringMap;
        private GameBoard Board;

        //creates deep copy of board
        public GameBoard CreateBoard(GameBoard board)
        {
            Board = new GameBoard();
            Board.Size = board.Size;
            Board.Width = board.Width;
            Board.Height = board.Height;
            Board.NumberOfPawnsPerPlayer = board.NumberOfPawnsPerPlayer;
            Board.Fields = new Field[Board.Height, Board.Width];
            Board.Bases = (Base[])board.Bases.Clone();

            for (int h = 0; h != board.Height; h++)
            {
                for (int w = 0; w != board.Width; w++)
                {
                    Pawn pawn = board.Fields[h, w].OccupiedBy;
                    Pawn newPawn = null;
                    if (pawn != null) {
                        newPawn = new Pawn(pawn.Owner, pawn.Y, pawn.X, pawn.Id, Board);
                        Board.Pawns.Add(newPawn);
                    }
                    Field field = board.Fields[h, w];
                    Board.Fields[h, w] = new Field(field.X, field.Y, field.Accessible, field.PlayersBase, Board, newPawn);
                    Board.Fields[h, w].BaseOrder = field.BaseOrder;
                }
            }
            return Board;
        }

        //creates board with given size
        public GameBoard CreateBoard(BoardSize size)
        {
            Board = new GameBoard();
            Board.Size = size;
            GenerateMapString(size);
            PopulateFields();
            return Board;
        }

        private void GenerateMapString(BoardSize size)
        {
            string[] map;
            switch (size)
            {
                case BoardSize.Small:
                    Board.Height = 9;
                    Board.Width = 13;
                    Board.NumberOfPawnsPerPlayer = 3;
                    map = new[]
                    {
                        "......1......",
                        ".....1.1.....",
                        "3.3.0.0.0.5.5",
                        ".3.0.0.0.0.5.",
                        "..0.0.0.0.0..",
                        ".6.0.0.0.0.4.",
                        "6.6.0.0.0.4.4",
                        ".....2.2.....",
                        "......2......"
                    };

                    break;
                case BoardSize.Medium:
                    Board.Height = 13;
                    Board.Width = 19;
                    Board.NumberOfPawnsPerPlayer = 6;
                    map = new[]
                    {
                        ".........1.........",
                        "........1.1........",
                        ".......1.1.1.......",
                        "3.3.3.0.0.0.0.5.5.5",
                        ".3.3.0.0.0.0.0.5.5.",
                        "..3.0.0.0.0.0.0.5..",
                        "...0.0.0.0.0.0.0...",
                        "..6.0.0.0.0.0.0.4..",
                        ".6.6.0.0.0.0.0.4.4.",
                        "6.6.6.0.0.0.0.4.4.4",
                        ".......2.2.2.......",
                        "........2.2........",
                        ".........2........."
                    };
                    break;
                case BoardSize.Large:
                    Board.Height = 17;
                    Board.Width = 25;
                    Board.NumberOfPawnsPerPlayer = 10;
                    map = new[]
                    {
                        "............1............",
                        "...........1.1...........",
                        "..........1.1.1..........",
                        ".........1.1.1.1.........",
                        "3.3.3.3.0.0.0.0.0.5.5.5.5",
                        ".3.3.3.0.0.0.0.0.0.5.5.5.",
                        "..3.3.0.0.0.0.0.0.0.5.5..",
                        "...3.0.0.0.0.0.0.0.0.5...",
                        "....0.0.0.0.0.0.0.0.0....",
                        "...6.0.0.0.0.0.0.0.0.4...",
                        "..6.6.0.0.0.0.0.0.0.4.4..",
                        ".6.6.6.0.0.0.0.0.0.4.4.4.",
                        "6.6.6.6.0.0.0.0.0.4.4.4.4",
                        ".........2.2.2.2.........",
                        "..........2.2.2..........",
                        "...........2.2...........",
                        "............2............"
                    };
                    break;
                default:
                    throw new NotImplementedException("Unexpected board size");
            }
            StringMap = map;
        }

        private void PopulateFields()
        {
            Board.Fields = new Field[Board.Height, Board.Width];
            int[] playerPawnsCounter = new int[6];

            for (int h = 0; h != Board.Height; h++)
            {
                for (int w = 0; w != Board.Width; w++)
                {
                    var currentField = StringMap[h][w];
                    switch (currentField)
                    {
                        case '.':
                            Board.Fields[h, w] = new Field(w, h, false, -1, Board);
                            break;
                        case '0':
                            Board.Fields[h, w] = new Field(w, h, true, -1, Board);
                            break;
                        default:
                            int baseNumber;
                            if (Int32.TryParse(currentField.ToString(), out baseNumber))
                            {
                                Board.Fields[h, w] = new Field(w, h, true, -1, Board);
                                Board.Bases[baseNumber - 1].Fields.Add(Board.Fields[h, w]);
                            }
                            else
                            {
                                throw new NotImplementedException("Field creation exception!");
                            }
                            break;
                    }
                }
            }

            //sortowanie pól żeby pionki lepiej wchodziły i 
            for (int i=0;i<6;i++)
            {
                foreach (var f in Board.Bases[i].Fields)
                {
                    if (i == 0) f.BaseOrder = f.Y;
                    else if (i == 1) f.BaseOrder = -f.Y;
                    else if (i == 2) f.BaseOrder = f.Y + f.X;
                    else if (i == 3) f.BaseOrder = -(f.Y + f.X);
                    else if (i == 4) f.BaseOrder = f.Y - f.X;
                    else if (i == 5) f.BaseOrder = f.X - f.Y;
                }
                
            }
            
 /*           
            if (Board.Size == BoardSize.Large)
            {
                Board.Bases[1].Fields.Clear();
                foreach (var f in Board.Bases[0].Fields)
                    Board.Bases[1].Fields.Add(f);

                Board.Bases[1].Fields.Remove(Board.Bases[1].Fields.Where(f => f.X == 15 && f.Y == 3).FirstOrDefault());
                //Board.Bases[1].Fields.Remove(Board.Bases[1].Fields.Where(f => f.X == 14 && f.Y == 2).FirstOrDefault());
                //Board.Bases[1].Fields.Add(Board.Fields[4, 10]);
                Board.Bases[1].Fields.Add(Board.Fields[4, 14]);
            }*/
        }
    }
}