﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace Trylma.Helpers.SignalR
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions{
                AuthenticationType = "ApplicationCookie",
                LoginPath = new PathString("/authorization/login")
            });

            app.MapSignalR();
        }
    }
}