﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Trylma.Controllers;
using Trylma.Models;

namespace Trylma.Helpers.SignalR
{
    public class GameHub : Hub
    {
        private IHubContext context = GlobalHost.ConnectionManager.GetHubContext<GameHub>();

        public void Started(int gameId)
        {
            string userName = Context.User.Identity.Name;
            Debug.WriteLine(userName + " started");
            Game game = Platform.GetGameById(gameId, userName);
            if (game != null && game.State == GameState.Started)
            {
                GameClient gc = game.Clients.Where(c => c.User.Name == userName).FirstOrDefault();
                lock (game)
                {
                    gc.Started = true;
                    game.TryBegin();
                }
            }
        }

        public override Task OnConnected()
        {
            string userName = Context.User.Identity.Name;
            Debug.WriteLine(userName + " has connected");

            Platform.OnConnect(userName);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string userName = Context.User.Identity.Name;
            Debug.WriteLine(userName + " has disconected");

            Platform.OnDisconnect(userName);

            return base.OnDisconnected(stopCalled);
        }

        //public 

        //wysyłane po skończeniu animacji kompa lub ruchu gracza
        public void EndMoveRequest(int gameId)
        {
            string userName = Context.User.Identity.Name;
            Game game = Platform.GetGameById(gameId, userName);
            if (game != null)
            {
                GameClient c = game.Clients.First(cl => cl.User.Name == userName);

                lock (this.GetType())
                {
                    c.EndedMove++;
                    Debug.WriteLine(userName + " EndMove " + c.EndedMove);
                    game.TryEndMove(userName, c.EndedMove);
                }
            }
        }

        public void MovePawnRequest(Field from, Field to, int gameId)
        {
            GameMove move;
            string userName = Context.User.Identity.Name;
            Game game = Platform.GetGameById(gameId, userName);
            if (game != null)
            {
                if ((move = game.TryMovePawn(from, to)) != null)
                {
                    game.Move++;
                    OnMove(move, game);
                }
            }
            else
            {
                Debug.WriteLine("MovePawnRequest on corrupted gameId = {0}", gameId);
            }
        }

        //wiadomości wysyłane z servera

        //->
        public void BeginMove(Game game)
        {
            Debug.WriteLine("BeginMove " + game.Move);
            if (game.Clients != null)
            {
                foreach (var client in game.Clients)
                {
                    context.Clients.User(client.User.Name).beginMove(game.GameId, game.CurrentPlayer.Id, game.Turn, game.Move);
                }
            }
        }

        public void GameOver(Game game, int winner)
        {
            if (game.Clients != null)
            {
                foreach (var client in game.Clients)
                {
                    context.Clients.User(client.User.Name).gameOver(game.GameId, winner);
                }
            }
        }

        public void SynchronizeRequest(int gameId)
        {
            string userName = Context.User.Identity.Name;
            Debug.WriteLine(userName + " request synchronize");
            Game game = Platform.GetGameById(gameId, userName);
            if (game != null && game.State == GameState.Started)
            {
                var PlayersDescs = game.Players.Select(x =>
                {
                    return new GameDesc.PlayerDesc(x);
                }).ToArray();
                context.Clients.User(userName).refreshBoard(game.GameId, PlayersDescs, game.GameBoard.Pawns, game.Move);
            }
        }

        //->
        public void RefreshBoard(GameRoom room)
        {
            var PlayersDescs = room.Game.Players.Select(x =>
            {
                return new GameDesc.PlayerDesc(x);
            }).ToArray();
            foreach (GameClient c in room.Game.Clients)
            {
                context.Clients.User(c.User.Name).refreshBoard(room.Game.GameId, PlayersDescs, room.Game.GameBoard.Pawns, room.Game.Move);
            }
        }

        public void OnMove(GameMove move, Game game)
        {
            Debug.WriteLine("OnMove");
            foreach (var client in game.Clients)
            {
                Debug.WriteLine("Sends move to " + client.User.Name);
                context.Clients.User(client.User.Name).move(game.GameId, move);
            }
        }

        public void OnMessage(string markup, GameRoom room)
        {
            foreach (GameClient c in room.Game.Clients)
            {
                context.Clients.User(c.User.Name).onMessage(room.GameId, markup);
            }
        }

        //wysłanie spersonalizowanego htmla
        public void UpdateRoom(GameRoom room, string username, string markup)
        {
            context.Clients.User(username).updateRoom(room.GameId, markup);
        }

        public void SendGameArea(string username, string markup, Game game)
        {
            Debug.WriteLine("SendGameArea");
            context.Clients.User(username).gameArea(game.GameId, markup, username, game.GetDesc());
        }

        public void OnRoomChange(GameRoom room, int newplid, int newuid)
        {
            foreach (GameClient c in room.Game.Clients)
            {
                context.Clients.User(c.User.Name).onRoomChange(room.GameId, newplid, newuid);
            }
        }

        public void OnStartGame(GameRoom room)
        {
            Debug.WriteLine("OnStartGame");
            foreach (GameClient c in room.Game.Clients)
            {
                context.Clients.User(c.User.Name).onStartGame(room.GameId);
            }
        }

        public void OnGameOver(Game game)
        {
            Debug.WriteLine("OnGameOver");
            foreach (GameClient c in game.Clients)
            {
                context.Clients.User(c.User.Name).onGameOver(game.GameId);
            }
        }
    }
}