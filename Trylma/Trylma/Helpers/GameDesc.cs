﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trylma.Models
{
    public class GameDesc
    {
        public class PlayerDesc
        {
            public int Type;
            public string Name;
            public string UserName;
            public int Id;
            public int Index;
            public int StartBase;
            public int EndBase;

            public PlayerDesc(Player p)
            {
                Type = (int)p.Type;
                Name = p.Name;
                UserName = p.User.Name;
                Id = p.Id;
                Index = p.Index;
                StartBase = p.StartBase;
                EndBase = p.EndBase;
            }
        }

        public int GameId;
        public int PlayersNum;
        public int CurrentPlayer;
        public int PawnsNum;
        public int BoardSize;
        public int BoardHeight;
        public int BoardWidth;
        public PlayerDesc[] PlayersDescs;
        public Pawn[] Pawns;
    }
}