﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trylma.Helpers
{
    public class Vector2D
    {
        public double x;
        public double y;

        public Vector2D(double u, double v) 
        {
            x = u;
            y = v;
        }

        public double Det(Vector2D v)
        {
            return x * v.y - v.x * y;
        }

        public void Scale(double sx, double sy)
        {
            x *= sx;
            y *= sy;
        }

        public double Length() 
        {
	        return Math.Sqrt(x*x+y*y);
        }


        public void Normalize() 
        {
	        var len = Math.Sqrt(x*x+y*y);
	        x /= len;
	        y /= len;
        }

        public double DotProduct(Vector2D v) {
	        return x*v.x + y*v.y;
        }

        public double CosBeetween(Vector2D v) {
	        return DotProduct(v) / (Length()*v.Length());
        }

        public Vector2D ProjectionOnto(Vector2D v)
        {
            double scalarProj = this.DotProduct(v) / v.Length();
            v.Normalize();
            return v * (double)scalarProj;
        }

        public static Vector2D operator-(Vector2D v, Vector2D v2)
        {
            return new Vector2D(v.x - v2.x, v.y - v2.y);
        }

        public static Vector2D operator*(Vector2D v, double s)
        {
            return new Vector2D(v.x*s, v.y*s);
        }

        public static Vector2D operator +(Vector2D v, Vector2D v2)
        {
            return new Vector2D(v.x + v2.x, v.y + v2.y);
        }

        public bool IsEqual(Vector2D v, double epsilon)
        {
            return Math.Abs(v.x - v.x) <= epsilon && Math.Abs(v.y - v.y) <= epsilon;
        }


    }
}