﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trylma.Models
{
    public class Pawn
    {
        GameBoard Board;

        public Pawn(int owner, int startingY, int startingX, int id, GameBoard board)
        {
            Owner = owner;
            X = startingX;
            Y = startingY;
            Id = id;
            Board = board;
        }

        public int Owner { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Id { get; set; }

        public List<Field> GetAvailableFields()
        {
            List<Field> moves = new List<Field>();
            Board.Fields[Y, X].GetAvailableFields(new List<Field>(), true, moves);
            return moves;
        }

        public Field GetOcuppiedField()
        {
            return Board.Fields[Y, X];
        }

        public void Move(Field f)
        {
            Field o = GetOcuppiedField();
            o.OccupiedBy = null;
            f.OccupiedBy = this;
            UpdatePawn(f.X, f.Y);
        }

        public void UpdatePawn(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}