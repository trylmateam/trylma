﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Trylma.Helpers;

namespace Trylma.Models
{
    public class GameRoom
    {
        public List<User> UserQueue;
        public List<Message> Messages;
        public Game Game;
        public User Owner;
        public int GameId;

        public GameRoom(User user)
        {
            Owner = user;
            UserQueue = new List<User>();
            Messages = new List<Message>();
            UserQueue.Add(DBContext.GetUserByLogin("HeuristicAI"));
            UserQueue.Add(DBContext.GetUserByLogin("MinMaxAI"));
            GameId = Platform.Games.Count == 0 ? 1 : Platform.Games.FindLast(game => game.GameId < int.MaxValue).GameId + 1; //TODO: Find a way to get first non-existing number in set
            Game = new Game(GameId, user, this);
        }

        public void Open()
        {
            UserQueue.Clear();
            Messages.Clear();
            Messages.Add(new Message(Owner, "[Otwarcie gry]", special: true));
            Game.Open();
        }

        public bool UserParticipate(string userName)
        {
            bool result = !(UserQueue.Where(u => u.Name == userName).FirstOrDefault() == null && Game.HasUser(userName) == false);
            return result;
        }

        public IEnumerable<User> GetParticipants()
        {
            return Game.Players.Where(p => p.Type == PlayerType.Human).Select(p => p.User).Concat(UserQueue);
        }

        public void SetReady(string username)
        {
            Player pl = Game.Players.Where(p => p.User.Name == username).FirstOrDefault();
            if (pl != null)
            {
                pl.Ready = true;
                if (AllPlayersReady()) Game.Start();
                Game.GameHub.OnRoomChange(this, pl.Id, -1);
            }
        }

        public bool AllPlayersReady()
        {
            return Game.Players.Where(pl => pl.Ready == false).FirstOrDefault() == null;
        }

        public bool Join(User user) 
        {
            if (Game.State != GameState.Started && UserQueue.Where(u => u.Name == user.Name).FirstOrDefault() == null)
                UserQueue.Add(user);

            if (Game.Clients.Where(c => c.User.Name == user.Name).FirstOrDefault() == null)
            {
                Game.Clients.Add(new GameClient { User = user, EndedMove = Game.Move });
                Game.GameHub.OnRoomChange(this, -1, user.Id);
                return true;
            }

            Game.UserJoinGame(user.Name);

            if (Game.State == GameState.Started)
            {
                Game.NextMove();
            }

            Game.GameHub.OnRoomChange(this, -1, user.Id);
            return false;
        }

        public bool IsInactive()
        {
            if (Game.IsGameOver)
            {
                foreach (var c in Game.Clients)
                {
                    if (c.Active == true) return false;
                }
                return true;
            }
            return false;
        }

        public bool Leave(User user)
        {
            Game.UserLeftGame(user.Name);
            if (UserParticipate(user.Name) == true)
            {
                if (Game.State == GameState.Opened)
                {
                    UserQueue.Remove(UserQueue.Where(u => u.Name == user.Name).FirstOrDefault());
                    Player player = Game.Players.Where(pl => pl.User == user).FirstOrDefault();

                    if (player != null)
                        Game.RemovePlayer(player.Id);
                }

                Game.GameHub.OnRoomChange(this, -1, user.Id);
                return true;
            }
            Game.GameHub.OnRoomChange(this, -1, -1);
            return false;
        }

        public void AddPlayer(User us)
        {
            if (Game.AddPlayer(us) == true)
            {
                if (Game.State == GameState.Opened)
                {
                    UserQueue.Remove(UserQueue.Where(u => u.Name == us.Name).FirstOrDefault());
                }
                Game.GameHub.OnRoomChange(this, Game.Players.Last().Id, -1);
            }
        }

        public void RemovePlayer(int playerId)
        {
            User user = Game.RemovePlayer(playerId);
            if (user != null)
            {
                if (Game.State == GameState.Opened)
                {
                    Game.Room.UserQueue.Add(user);
                }
                Game.GameHub.OnRoomChange(Game.Room, -1, user.Id);
            }
        }


        public void AddMessage(Message msg)
        {
            Messages.Add(msg);
        }
    }
}