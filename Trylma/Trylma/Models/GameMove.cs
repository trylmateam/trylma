﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Trylma.Helpers;

namespace Trylma.Models
{
    public class GameMove
    {
        public Pawn Pawn;
        public Field From;
        public Field To;

        public GameMove()
        {
        }

        public GameMove(Pawn p, Field f, Field t)
        {
            From = f;
            To = t;
            Pawn = p;
        }

        public Vector2D GetDisplacementVector()
        {
            return new Vector2D(To.X - From.X, To.Y - From.Y);
        }

        public bool EnterBase()
        {
            return From.PlayersBase != Pawn.Owner && To.PlayersBase == Pawn.Owner;
        }

        public bool LeaveBase()
        {
            return From.PlayersBase == Pawn.Owner && To.PlayersBase != Pawn.Owner;
        }

        public bool InsideBase()
        {
            return From.PlayersBase == Pawn.Owner && To.PlayersBase == Pawn.Owner;
        }
    }
}