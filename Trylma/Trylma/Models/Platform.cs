﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Trylma.Helpers;
using Trylma.Models;

namespace Trylma.Models
{
    static public class Platform
    {
        static public List<GameRoom> Games = new List<GameRoom>();
        static public List<Client> ConnectedClients = new List<Client>();

        static public void OnConnect(string username)
        {
            if (username == "") return;

            Client client = ConnectedClients.Where(c => c.User.Name == username).FirstOrDefault();
            
            if (client == null)
            {
                User u = DBContext.GetUserByLogin(username);
                if (u != null)
                {
                    client = new Client()
                    {
                        User = u,
                        Connections = 0
                    };
                }
                else
                {
                    Debug.WriteLine("OnConnect: User not found in database! "+username);
                }

                ConnectedClients.Add(client);
            }
            client.Connections++;
        }

        static public void OnDisconnect(string username)
        {
            if (username == "") return;

            Client client = ConnectedClients.FirstOrDefault(c => c.User.Name == username);
            if (client != null)
            {
                client.Connections--;
                if (client.Connections == 0) OnClientLeave(client);
            }
        }

        static public void Logout(string username)
        {
            if (username == "") return;

            Client client = ConnectedClients.FirstOrDefault(c => c.User.Name == username);
            if (client != null)
            {
                OnClientLeave(client);
                ConnectedClients.Remove(client);
            }
        }

        static private void OnClientLeave(Client client)
        {
            GetAllUserRooms(client.User).ForEach(g => g.Leave(client.User));
        }

        static private List<GameRoom> GetAllUserRooms(User user)
        {
            List<GameRoom> rooms = Games.Where(r => r.UserParticipate(user.Name)).ToList();
            return rooms;
        }

        public static Game GetGameById(int id, string userName)
        {
            GameRoom room = GetRoomById(id, userName);
            return room.Game;
        }

        public static GameRoom GetRoomById(int id, string userName)
        {
            GameRoom room = Games.FirstOrDefault(r => r.GameId == id);

            if (room == null)
            {
                throw new Errors.GameNotFound(id);
            }

            if (room.Game.State == GameState.Configured && room.Game.Owner.Name != userName)
            {
                throw new Errors.GameNotFound(id);
            }

            return room;
        }
    }
}