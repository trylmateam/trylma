﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trylma.Models
{
    public class RoomTable
    {
        public GameRoom Room;
        public User NewUser;
        public Player NewPlayer;

        public RoomTable() { }
        public RoomTable(GameRoom gr, User nu, Player np)
        {
            NewUser = nu;
            NewPlayer = np;
            Room = gr;
        }
    }
}