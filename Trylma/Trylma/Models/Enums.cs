﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trylma.Models
{
    public enum BoardSize
    {
        Small = 0,
        Medium = 1,
        Large = 2
    }

    public enum GameState
    {
        Configured,
        Opened,
        Started,
    };

    public enum GameMode
    {
        FirstWin, // gra kończy sie jak skończy pierwszy
        Standard // gra kończy sie jak wszyscy skończą (było potrzebne do testów i liczenia ruchów)
    };

    public enum PlayerType
    {
        Human = 0, 
        HeuristicAI = 1, 
        MinMaxAI = 2
    };
}