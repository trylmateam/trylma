﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Trylma.Helpers;

namespace Trylma.Models
{
    public class Field
    {
        GameBoard Board;

        public Pawn OccupiedBy;
        public int PlayersBase;
        public int X, Y;
        public int BaseOrder;

        public Field(int x, int y, bool accessibile, int playersBase, GameBoard board, Pawn occupiedBy = null)
        {
            Accessible = accessibile;
            PlayersBase = playersBase;
            OccupiedBy = occupiedBy;
            X = x;
            Y = y;
            Board = board;
        }

        public bool Accessible;

        public bool Occupied
        {
            get { return OccupiedBy != null; }
        }

        public Vector2D GetPos()
        {
            return new Vector2D(X, Y);
        }

        public bool Equals(Field f)
        {
            return X == f.X && Y == f.Y && Board == f.Board;
        }

        static private int[,] neighbours = { { -1, 1 }, { 1, 1 }, { -1, -1 }, { 1, -1 }, { 2, 0 }, { -2, 0 } };
        public List<Field> GetAvailableFields(List<Field> visited, bool firstStep, List<Field> returnList)
        {
            //List<Field> returnList = new List<Field>();
            visited.Add(this);

            for (int n = 0; n < 6; n++)
            {
                if (Board.InBoard(X + neighbours[n, 0], Y + neighbours[n, 1]))
                {
                    Field currentlyCheckedField = Board.Fields[Y + neighbours[n, 1], X + neighbours[n, 0]];
                    if (currentlyCheckedField.Accessible && visited.FindIndex(f => f.Y == Y + neighbours[n, 1] && f.X == X + neighbours[n, 0]) == -1)
                    {
                        if (!currentlyCheckedField.Occupied && firstStep)
                        {
                            returnList.Add(currentlyCheckedField);
                        }
                        else if (currentlyCheckedField.Occupied)
                        {
                            if (Board.InBoard(X + neighbours[n, 0] * 2, Y + neighbours[n, 1] * 2))
                            {
                                currentlyCheckedField = Board.Fields[Y + neighbours[n, 1] * 2, X + neighbours[n, 0] * 2];
                                if (currentlyCheckedField.Accessible && !currentlyCheckedField.Occupied && visited.FindIndex(f => f.Y == Y + neighbours[n, 1] * 2 && f.X == X + neighbours[n, 0] * 2) == -1)
                                {
                                    returnList.Add(currentlyCheckedField);
                                    currentlyCheckedField.GetAvailableFields(visited, false, returnList);
                                }
                            }
                        }
                    }
                }
            }
            return returnList;
        }
    }
}