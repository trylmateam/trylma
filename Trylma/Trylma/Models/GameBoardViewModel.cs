﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trylma.Models
{
    public class GameBoardViewModel
    {
        public Field[,] Fields { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int NumberOfPawnsPerPlayer { get; set; }
        public int NumberOfPlayers { get; set; }
        public List<String> PlayerNames = new List<String>();

        public GameBoardViewModel(Game game)
        {
            Fields = game.GameBoard.Fields;
            Height = game.GameBoard.Height;
            Width = game.GameBoard.Width;
            NumberOfPawnsPerPlayer = game.GameBoard.NumberOfPawnsPerPlayer;
            //NumberOfPlayers = game.NumberOfPlayers;
            foreach (var player in game.Players)
            {
                PlayerNames.Add(player.Name);
            }
        }
    }
}