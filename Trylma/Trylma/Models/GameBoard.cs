﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using Trylma.AI;
using Trylma.Helpers;

namespace Trylma.Models
{
    public class Base
    {
        public List<Field> Fields = new List<Field>();
        private Vector2D massCenter = null;
        public Vector2D MassCenter
        {
            get
            {
                if (massCenter == null)
                {
                    massCenter = new Vector2D(
                        (double)Fields.Sum(f => f.X) / (double)Fields.Count(),
                        (double)Fields.Sum(f => f.Y) / (double)Fields.Count());
                }
                return massCenter;
            }
        }

        public int GetFieldIndex(Field field)
        {
            return Fields.FindIndex(f => f.X == field.X && f.Y == field.Y);
        }

        public Field GetFirstFieldInBaseNotOccupiedBy(Pawn pawn)
        {
            return Fields
                .Where(f => f.OccupiedBy != null && f.OccupiedBy.Owner != pawn.Owner || f.OccupiedBy == null)
                .OrderBy(f => f.BaseOrder)
                .ThenBy(f => new Vector2D(pawn.X - f.X, pawn.Y - f.Y).Length())
                .FirstOrDefault();
        }

        public Field GetNearestFieldInBaseNotOccupiedBy(Pawn pawn)
        {
            return Fields
                .Where(f => f.OccupiedBy != null && f.OccupiedBy.Owner != pawn.Owner || f.OccupiedBy == null)
                .OrderBy(f => new Vector2D(pawn.X - f.X, pawn.Y - f.Y).Length())
                .FirstOrDefault();
        }

        public int GetNumPawnsOfPlayer(int playerId)
        {
            return Fields.Sum(f => f.OccupiedBy != null && f.OccupiedBy.Owner == playerId ? 1 : 0);
        }
    };

    public class GameBoard
    {
        public List<Pawn> Pawns = new List<Pawn>();
        public Field[,] Fields;
        public int Height, Width;
        public int NumberOfPawnsPerPlayer;
        public Base[] Bases = new Base[6];
        public BoardSize Size;

        public GameBoard()
        {
            for (int i = 0; i < Bases.Length; i++)
            {
                Bases[i] = new Base();
            }
        }

        public Pawn GetPawnByPlayerAndIndex(int playerid, int pawnid)
        {
            return Pawns.Where(pawn => pawn.Owner == playerid && pawn.Id == pawnid).First();
        }

        //bierze playera i tworzy jego pionki zgodnie z jego startbase
        public void CreatePlayerPawns(Player player)
        {
            player.Pawns.Clear();

            int pawnId = 0;
            foreach (var field in Bases[player.StartBase - 1].Fields)
            {
                Pawn p = new Pawn(player.Id, field.Y, field.X, pawnId++, this);
                field.OccupiedBy = p;
                player.Pawns.Add(p);
                Pawns.Add(p);
            }

            //ustawienie właściwości playerBase dla pól
            foreach (var field in Bases[player.EndBase - 1].Fields)
            {
                field.PlayersBase = player.Id;
            }
        }

        public void RemovePlayerPawns(Player player)
        {
            foreach (var field in Bases[player.EndBase - 1].Fields)
            {
                field.PlayersBase = -1;
            }

            foreach (Pawn p in player.Pawns)
            {
                Pawns.Remove(p);
                p.GetOcuppiedField().OccupiedBy = null;
            }
        }

        public bool InBoard(int X, int Y)
        {
            return X >= 0 && Y >= 0 && X < Width && Y < Height;
        }

        public static List<Pawn> SortPawns(List<Pawn> pawns, int baseId)
        {
            switch (baseId)
            {
                case 0: return pawns.OrderBy(f => f.Y).ThenBy(f => f.X).ToList();
                case 1: return pawns.OrderByDescending(f => f.Y).ThenByDescending(f => f.X).ToList();
                case 2: return pawns.OrderBy(f => f.X + f.Y).ThenByDescending(f => f.Y).ToList();
                case 3: return pawns.OrderByDescending(f => f.X + f.Y).ThenBy(f => f.Y).ToList();
                case 4: return pawns.OrderByDescending(f => f.X - f.Y).ThenBy(f => f.Y).ToList();
                case 5: return pawns.OrderBy(f => f.X - f.Y).ThenByDescending(f => f.Y).ToList();
            }

            return null;
        }

        public static List<Field> SortFields(List<Field> fields, int baseId)
        {
            switch (baseId)
            {
                case 0: return fields.OrderBy(f => f.Y).ThenBy(f => f.X).ToList();
                case 1: return fields.OrderByDescending(f => f.Y).ThenByDescending(f => f.X).ToList();
                case 2: return fields.OrderBy(f => f.X + f.Y).ThenByDescending(f => f.Y).ToList();
                case 3: return fields.OrderByDescending(f => f.X + f.Y).ThenBy(f => f.Y).ToList();
                case 4: return fields.OrderByDescending(f => f.X - f.Y).ThenBy(f => f.Y).ToList();
                case 5: return fields.OrderBy(f => f.X - f.Y).ThenByDescending(f => f.Y).ToList();
            }

            return null;
        }


        public static Vector2D FlattenVector(Vector2D v, Player pl, double sx = 2.0, double sy = 1.15470053838)
        {
            v.Scale(0.5, Math.Sqrt(3) / 2.0);

            Vector2D startBase = pl.GetStartBase().MassCenter;
            Vector2D endBase = pl.GetEndBase().MassCenter;

            Vector2D baseAxis = endBase - startBase;
            baseAxis.Scale(0.5, Math.Sqrt(3) / 2.0);
            
            Vector2D ortoAxis = new Vector2D(baseAxis.y, -baseAxis.x);

            Vector2D xComponent = v.ProjectionOnto(ortoAxis);
            Vector2D yComponent = v.ProjectionOnto(baseAxis);

            xComponent *= sx;
            yComponent *= sy;

            v = xComponent + yComponent;
            return v;
        }
    }
}