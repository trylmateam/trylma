﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trylma.Models
{
    public class Client
    {
        public int Connections { get; set; }
        public User User { get; set; }
    }

    public class GameClient
    {
        public User User { get; set; }
        public int EndedMove = 0;
        public bool Started = false;
        public bool Active = true;
    }
}