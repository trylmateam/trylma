﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trylma.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AvatarPath { get; set; }
        public PlayerType PlayerType { get; set; }
        public string PasswordHash { get; set; }
        public int Ranking { get; set; }

        public User() { }

        public User(int id, string name, string passhash, int type, int ranking, string avPath = "/Resources/Images/Avatars/default.jpg")
        {
            Id = id;
            Name = name;
            AvatarPath = avPath;
            PasswordHash = passhash;
            PlayerType = (PlayerType)type;
            Ranking = ranking;
        }

        public static User HeuristicAIUser;
        public static User MinMaxAIUser;
    }
}