﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trylma.Models
{
    public class Message
    {
        public User Sender { get; private set; }
        public string Msg { get; private set; }
        public string Time { get; private set; }
        public bool Special { get; private set; }

        public Message(User u, string m, bool special = false)
        {
            Sender = u;
            Msg = m;
            Special = special;
            Time = String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now);
        }
    }
}