﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

using Trylma.AI;
using Trylma.Helpers;

namespace Trylma.Models
{
    public class Player
    {
        public static string[] Colors = { "red", "green", "blue", "darkOrange", "black", "white" };

        public Player(User user, int id, PlayerType type, int startBase, Game game, string name="", int endBase = -1)
        {
            if (name == "")
                Name = user.Name;
            else Name = name;

            User = user;
            Pawns = new List<Pawn>();
            isHuman = type == PlayerType.Human;
            Type = type;
            StartBase = startBase;  
            EndBase = endBase == -1 ? (startBase % 2 == 0 ? startBase - 1 : startBase + 1) : endBase;
            Game = game;
            EndedMove = 0;

            if (type == PlayerType.HeuristicAI) AI = new AI.HeuristicAI();
            else AI = new AI.HeuristicMinMaxAI(Game.Players.Count, Game);

            if (type != PlayerType.Human) Ready = true;
            
            Id = id;
            Index = 0;
        }
 
        public bool Ready;
        public string Color;
        public User User;
        public Game Game;
        public PlayerType Type;
        public int Id;
        public int Index;
        public string Name;
        public List<Pawn> Pawns;
        public int NumberOfPawnsInBase;
        public bool isHuman;
        public AI.IArtificallIntelligence AI;
        public List<Field> Base;
        public int StartBase;
        public int EndBase;
        public bool Finished;
        public int EndedMove; //ostatni ruch jaki przetworzył klient
        public int Moves = 0;
        public int IncreaseOfRanking = 0;

        public void SetBases(int start) {
            StartBase = start;
            EndBase = (StartBase % 2 == 0 ? StartBase - 1 : StartBase + 1);
        }

        public bool IsConnected()
        {
            if (Type == PlayerType.Human)
                return Game.Clients.Where(cl => cl.User.Name == User.Name && cl.Active == true).FirstOrDefault() != null;
            return true;
        }

        public Base GetStartBase()
        {
            return Game.GameBoard.Bases[StartBase-1];
        }

        public Base GetEndBase()
        {
            return Game.GameBoard.Bases[EndBase-1];
        }

        public GameMove ArtificalMove()
        {
            GameMoveNode node = AI.Move(this);
            if (node != null)
                return new GameMove(node.Pawn, node.From, node.To);
            return null;
        }

        public bool MovePawn(Pawn pawn, int x, int y)
        {
            bool didMove = false;
            var listOfPossibilities = pawn.GetAvailableFields();
            if (listOfPossibilities.Count > 0)
            {
                Field oldField = Game.GameBoard.Fields[pawn.Y, pawn.X];
                Field selectedField = Game.GameBoard.Fields[y, x];

                if (listOfPossibilities.Contains(selectedField))
                {
                    oldField.OccupiedBy = null;
                    selectedField.OccupiedBy = pawn;
                    pawn.UpdatePawn(selectedField.X, selectedField.Y);
                    Moves++;
                    didMove = true;
                }
                else
                {
                    didMove = false;
                }

                if (oldField.PlayersBase == pawn.Owner && selectedField.PlayersBase != pawn.Owner)
                {
                    Game.Players[pawn.Owner].NumberOfPawnsInBase--;
                }
                else if (oldField.PlayersBase != pawn.Owner && selectedField.PlayersBase == pawn.Owner)
                {
                    Game.Players[pawn.Owner].NumberOfPawnsInBase++;
                }
            }
            return didMove;
        }
    }
}