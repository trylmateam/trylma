﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trylma.Models
{
    public class Errors
    {
        public class GameNotFound : Exception  
        {
            private int gameId;

            public GameNotFound(int id)
            {
                gameId = id;
            }

            public override string Message
            {
                get
                {
                    return "Nie znaleziono gry o identyfikatorze " + gameId;
                }
            }
        }
    }
}