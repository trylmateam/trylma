﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Trylma.AI;
using Trylma.Helpers;
using Trylma.Helpers.SignalR;

namespace Trylma.Models
{
    public class Game
    {
        public User Owner { get; private set; }
        public GameState State { get; private set; }

        public int GameId { get; private set; }
        public GameHub GameHub;

        public GameMode Mode;
        public GameBoard GameBoard;
        public List<Player> Players;
        public GameRoom Room;
        public List<GameClient> Clients; //można nie być graczem a byc klientem, np. jak odpali się grę samych komputerów
        public Random RNG;
        public int CurrentPlayerId;
        public bool IsGameOver;
        public int Turn;
        public int Move;
        public List<Player> Winners = new List<Player>();

        private bool begun;

        public Game(int id, User user, GameRoom room)
        {
            Mode = GameMode.FirstWin;
            GameHub = new Helpers.SignalR.GameHub();
            State = GameState.Configured;
            Owner = user;
            Turn = 1;
            Move = 0;
            IsGameOver = false;
            RNG = new Random();
            CurrentPlayerId = 0;
            Players = new List<Player>();
            Clients = new List<GameClient>();
            SetBoardSize(BoardSize.Medium);
            GameId = id;
            Room = room; 
        }
         
        public void Open()
        {
            State = GameState.Opened;
        }

        public void Start()
        {
            Debug.WriteLine("-----------Game started-------------");
            State = GameState.Started;
            //Room.UserQueue.Clear();

            GameHub.OnStartGame(Room);
        }

        public Player CurrentPlayer
        {
            get { return Players.Where(pl => pl.Id == CurrentPlayerId).FirstOrDefault(); }
        }

        public void UserLeftGame(string userName)
        {
            lock (this)
            {
                Clients.First(c => c.User.Name == userName).Active = false;
            }
        }

        public void UserJoinGame(string userName)
        {
            GameClient client = Clients.Where(cl => cl.User.Name == userName).FirstOrDefault();
            if (client != null)
            {
                client.Active = true;
                if (client.EndedMove != Move)
                {
                    client.EndedMove = Move;
                    //TryEndMove(userName, client.EndedMove);
                }
            }
        }

        public bool HasUser(string userName)
        { 
            return !(Players.Where(pl => pl.User.Name == userName).FirstOrDefault() == null);
        }

        public bool AddPlayer(User user)
        {
            if (State == GameState.Started) return false;

            if (Players.Count() < 6)
            {
                Player newPlayer; 
                if (Players.Count > 0) {
                    Player lastPlayer = Players[Players.Count-1];
                    newPlayer = new Player(user, lastPlayer.Id+1, user.PlayerType, lastPlayer.StartBase+1, this, user.Name);
                }
                else {
                    newPlayer = new Player(user, 0, user.PlayerType, 1, this, user.Name);
                }
                Players.Add(newPlayer);
                PlacePlayers();
                //Room.AddMessage(new Message(Owner, "[Dodanie gracza "+finalName+"]"));
                return true;
            }
            return false;
        }

        private void PlacePlayers() {
            if (Players.Count == 1)
            {
                Players[0].SetBases(2); 
            }
            else if (Players.Count == 2)
            {
                Players[0].SetBases(2);
                Players[1].SetBases(1);
            }
            else if (Players.Count == 3)
            {
                Players[0].SetBases(1);
                Players[1].SetBases(6);
                Players[2].SetBases(4);
            }
            else if (Players.Count == 4)
            {
                Players[0].SetBases(3);
                Players[1].SetBases(4);
                Players[2].SetBases(5);
                Players[3].SetBases(6);
            }
            else if (Players.Count == 5)
            {
                Players[0].SetBases(1);
                Players[1].SetBases(3);
                Players[2].SetBases(4);
                Players[3].SetBases(5);
                Players[4].SetBases(6);
            }
            else if (Players.Count == 6)
            {
                Players[0].SetBases(1);
                Players[1].SetBases(2);
                Players[2].SetBases(3);
                Players[3].SetBases(4);
                Players[4].SetBases(5);
                Players[5].SetBases(6);
            }

            for (int i=0;i<Players.Count;i++)
            {
                Players[i].Index = i;
                Players[i].Color = Player.Colors[i];
            }
            CurrentPlayerId = Players.Count > 0 ? Players[0].Id : -1;

            SetBoardSize(GameBoard.Size);
        }

        public User RemovePlayer(int id)
        {
            if (State == GameState.Started) return null;

            Player player = Players.Where(pl => pl.Id == id).FirstOrDefault();
            if (player != null) 
            {
                Players.Remove(player);

                Players.Where(pl => pl.Id > id).ToList().ForEach(pl => pl.Id--);

                PlacePlayers();
                //Room.AddMessage(new Message(Owner, "[Usunięcie gracza "+player.Name+"]"));
                return player.User;
            }
            return null;
        }
        

        public void SetBoardSize(BoardSize boardSize)
        {
            if (State == GameState.Started) return;

            BoardFactory boardFactory = new BoardFactory();
            GameBoard = boardFactory.CreateBoard((BoardSize)boardSize);
            foreach (var player in Players)
            {
                GameBoard.CreatePlayerPawns(player);
            }
        }

        public GameMove TryMovePawn(Field from, Field to)
        {
            Debug.WriteLine("TryMovePawn");
            Pawn selectedPawn = CurrentPlayer.Pawns.Where(pawn => pawn.X == from.X && pawn.Y == from.Y).FirstOrDefault();
            if (selectedPawn != null)
            {
                if (CurrentPlayer.MovePawn(selectedPawn, to.X, to.Y) == true)
                {
                    return new GameMove(selectedPawn, from, to);
                }
            }
            return null;
        }

        public void NextPlayer()
        {
            if (IsGameOver) return;
            Debug.WriteLine("Next Player");
            do
            {
                CurrentPlayerId = (CurrentPlayerId + 1) % Players.Count;
            } while (CurrentPlayer.Finished == true);

            if (CurrentPlayerId == 0) NextTurn();
        }

        public void NextMove()
        {
            if (State == GameState.Started)
            {
                Debug.WriteLine("NextMove");
                GameHub.BeginMove(this);
                if (CurrentPlayer != null && !CurrentPlayer.isHuman)
                {
                    GameMove move = CurrentPlayer.ArtificalMove();
                    if (move != null)
                    {
                        Move++;
                        GameHub.OnMove(move, this);
                    }
                    else
                    {
                        //NextPlayer();
                        //NextMove();
                    }
                }
            }
        }

        private bool AllClientsEndedMove(int move)
        {
            /*
            Debug.WriteLine(Clients
                .Select(cl => cl.Player.Name+": "+cl.EndedMove)
                .Aggregate((current, next) => current + ", " + next));*/

            return Clients.Where(cl => cl.Active == true && cl.EndedMove < move).FirstOrDefault() == null;
        }

        private bool AllClientsStarted()
        {
            return Clients.Where(cl => cl.Started == false).FirstOrDefault() == null;
        }

        public void TryBegin()
        {
            if (AllClientsStarted() && begun == false)
            {
                begun = true;
                Debug.WriteLine("------ TryStart success! -------");
                NextMove();
            }
            else
            {
                Debug.WriteLine("TryStart fail!");
            }
        }

        //wywoływana z huba
        public void TryEndMove(string user, int move)
        {
            Debug.WriteLine(user+" TryEndMove "+move);
            if (move != Move)
            {
                Debug.WriteLine("Try End Move Exception!");
                return;
            }
            //jesli wszyscy skończyli ostatni ruch (doszło to do nich i wyswietlili animację)
            if (AllClientsEndedMove(Move) == true)
            {
                Debug.WriteLine("Move ended. Current move = "+Move);

                //sprawdzamy czy ktoś nie wygrał

                NextPlayer();

                if (!IsGameOver)
                {
                    NextMove();
                }
            }
        }

        public void NextTurn()
        {
            Turn++;
            CheckIsGameOver();
        }

        public void CheckIsGameOver()
        {
            foreach (var pl in Players)
            {
                if (pl.NumberOfPawnsInBase == GameBoard.NumberOfPawnsPerPlayer)
                {
                    pl.Finished = true;
                    IsGameOver = true;
                    Winners.Add(pl);
                }
            }
            if (IsGameOver)
            {
                UpdateRanking();
                GameHub.OnGameOver(this);
            }
        }

        private static int CalculateNewRanking(Player player, int suma, bool winner)
        {
            if (winner) return (int)(100.0 * (float)(suma - player.User.Ranking) / (float)suma);
            else return (int)(-(float)player.User.Ranking / (float)suma * 100.0);
        }

        private void UpdateRanking()
        {
            int totalityOfRanking = Players.Sum(p => p.User.Ranking);

            foreach (Player p in Players)
            {
                if (Winners.Contains(p))
                    p.IncreaseOfRanking = CalculateNewRanking(p, totalityOfRanking, true);
                else
                    p.IncreaseOfRanking = CalculateNewRanking(p, totalityOfRanking, false);

                p.User.Ranking += p.IncreaseOfRanking;

                DBContext.ChangeRanking(p.User);
            }
        }

        public Player GetPlayerById(int id)
        {
            return Players.Where(pl => pl.Id == id).FirstOrDefault();
        }

        public GameDesc GetDesc()
        {
            return new GameDesc
            {

                GameId = this.GameId,
                BoardWidth = GameBoard.Width / 2 + 1,
                BoardHeight = GameBoard.Height,
                BoardSize = (int)GameBoard.Size,
                PawnsNum = GameBoard.NumberOfPawnsPerPlayer,
                PlayersNum = Players.Count(),
                PlayersDescs = Players.Select(x =>
                {
                    return new GameDesc.PlayerDesc(x);
                }).ToArray(),
                Pawns = GameBoard.Pawns.ToArray(),
                CurrentPlayer = CurrentPlayerId
            };
        }
    }
}