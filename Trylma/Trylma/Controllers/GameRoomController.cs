﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Trylma.Helpers;
using Trylma.Models;

namespace Trylma.Controllers
{
    public class GameRoomController : Controller
    {
        [HttpGet]
        public ActionResult Create()
        {
            User user = DBContext.GetUserByLogin(User.Identity.Name);
            GameRoom room = new GameRoom(user);
            Platform.Games.Add(room);
            ViewBag.GameDesc = room.Game.GetDesc();
            return RedirectToAction("Index", new { room.GameId });
        }

        public ActionResult Index(int gameId)
        {
            try
            {
                Game game = Platform.GetGameById(gameId, User.Identity.Name);
                ViewBag.GameDesc = game.GetDesc();
                return View(game.Room);
            }
            catch (Errors.GameNotFound e) {
                return View("Error", e);
            }
        }

        public JsonResult Open(int gameId)
        {
            try 
            { 
                GameRoom room = Platform.GetRoomById(gameId, User.Identity.Name);
                room.Open();
                room.Game.GameHub.OnRoomChange(room, room.Game.Players.Last().Id, -1);
                return Json(true);
            }
            catch (Errors.GameNotFound e)
            {
                return Json(false);
            }
        }


        [HttpPost]
        public JsonResult AddMessage(int gameId, string msg)
        {
            try
            {
                GameRoom room = Platform.GetRoomById(gameId, User.Identity.Name);
                User user = DBContext.GetUserByLogin(User.Identity.Name);

                Message mess = new Message(user, msg);
                NewMessage(mess, room);
                return Json(true);
            }
            catch (Exception e)
            {
                return Json(false);
            }
        }


        [HttpPost]
        public JsonResult LeaveRoom(int gameId)
        {
            try
            {
                User me = DBContext.GetUserByLogin(User.Identity.Name);
                GameRoom room = Platform.GetRoomById(gameId, User.Identity.Name);

                room.Leave(me);
                if (room.IsInactive()) Platform.Games.Remove(room);
                NewMessage(new Message(me, "[Wychodzi]"), room);
                return Json(true);
            }
            catch (Exception e)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult JoinRoom(int gameId)
        {
            try
            { 
                User me = DBContext.GetUserByLogin(User.Identity.Name);
                GameRoom room = Platform.GetRoomById(gameId, User.Identity.Name);
                room.Join(me);
                NewMessage(new Message(me, "[Wchodzi]"), room);
                return Json(true);
            }
            catch (Exception e)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult AddPlayer(int gameId, string userName)
        {
            try 
            { 
                GameRoom room = Platform.GetRoomById(gameId, User.Identity.Name);
                User user = DBContext.GetUserByLogin(userName);
                room.AddPlayer(user);
            }
            catch (Exception e)
            {
                return Json(false);
            }
            return Json(true);
        }

        [HttpPost]
        public JsonResult RemovePlayer(int gameId, int playerId)
        {
            try
            {
                GameRoom room = Platform.GetRoomById(gameId, User.Identity.Name);
                room.RemovePlayer(playerId);
            }
            catch (Exception e)
            {
                return Json(false);
            }
            return Json(true);
        }

        [HttpPost]
        public JsonResult Update(int gameId, int newPlayerId, int newUserId)
        {
            try
            { 
                Game game = Platform.GetGameById(gameId, User.Identity.Name);
                UpdateRoomTable(game.Room, newPlayerId, newUserId);
                game.GameHub.RefreshBoard(game.Room);
                return Json(true);
            }
            catch (Errors.GameNotFound e) { 
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult SetReady(int gameId)
        {
            try
            {
                Debug.WriteLine(User.Identity.Name + " SetReady");
                GameRoom room = Platform.GetRoomById(gameId, User.Identity.Name);
                room.SetReady(User.Identity.Name);
                return Json(true);
            }
            catch (Exception e)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult GetGame(int gameId)
        {
            try
            {
                Debug.WriteLine(User.Identity.Name + " GetGame");
                GameRoom room = Platform.GetRoomById(gameId, User.Identity.Name);
                return Json(true);
            }
            catch (Exception e)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult GetGameOverInfo(int gameId)
        {
            try 
            { 
                GameRoom room = Platform.GetRoomById(gameId, User.Identity.Name);

                string html = PartialView("GameOver", room.Game.Players).RenderToString();
                return Json(html);
            }
            catch (Errors.GameNotFound e)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public JsonResult StartGame(int gameId)
        {
            try
            {
                Debug.WriteLine(User.Identity.Name + " StartGame");
                Game game = Platform.GetGameById(gameId, User.Identity.Name);
                if (game.State != GameState.Started)
                {
                    game.Start();
                    game.GameHub.OnRoomChange(game.Room, -1, -1);
                    return Json(true);
                }
                return Json(false);
            }
            catch (Errors.GameNotFound e)
            {
                return Json(false);
            }
        }


        //wysyłają partial views przez signalR
        private void SendGame(GameRoom room) 
        {
            string html = PartialView("GameHUD", room.Game).RenderToString();
            room.Game.GameHub.SendGameArea(User.Identity.Name, html, room.Game);
        }

        public JsonResult SetBoardSize(int gameId, int boardSize)
        {
            try
            {
                Game game = Platform.GetGameById(gameId, User.Identity.Name);
                game.SetBoardSize((BoardSize)boardSize);
                ViewBag.GameDesc = game.GetDesc();
                return Json(new
                {
                    result = true,
                    game = Json(game.GetDesc())
                });
            }
            catch (Errors.GameNotFound e)
            {
                return Json(new
                {
                    result = false,
                });
            }
        }

        private void UpdateRoomTable(GameRoom room, int newplid, int newuid)
        {
            if (room.Game.State == GameState.Configured)
            {
                RoomTable rt = new RoomTable
                {
                    Room = room,
                    NewUser = DBContext.GetUserById(newuid),
                    NewPlayer = room.Game.Players.Where(pl => pl.Id == newplid).FirstOrDefault()
                };
                string html = PartialView("Config", rt).RenderToString();
                room.Game.GameHub.UpdateRoom(room, User.Identity.Name, html);
            }
            else if (room.Game.State == GameState.Opened)
            {
                RoomTable rt = new RoomTable
                {
                    Room = room,
                    NewUser = DBContext.GetUserById(newuid),
                    NewPlayer = room.Game.Players.Where(pl => pl.Id == newplid).FirstOrDefault()
                };
                string html = PartialView("RoomTable", rt).RenderToString();
                room.Game.GameHub.UpdateRoom(room, User.Identity.Name, html);
            }
            else if (room.Game.State == GameState.Started)
            {
                SendGame(room);
            }
        }

        private void NewMessage(Message msg, GameRoom room)
        {
            room.AddMessage(msg);
            room.Game.GameHub.OnMessage(PartialView("_Message", msg).RenderToString(), room);
        }
    }
}
