﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trylma.Helpers;
using Trylma.Models;

namespace Trylma.Controllers
{
    public class PlatformController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("OpenedGames");
        }

        public ActionResult RankingList()
        {
            return View("RankingList", DBContext.GetRankingList());
        }

        public ActionResult OpenedGames()
        {
            ViewBag.GameState = (int)GameState.Opened;
            return View("ListOfGames", Platform.Games.Where(g => g.Game.State == GameState.Opened).ToList());
        }

        public ActionResult MyGames()
        {
            ViewBag.GameState = (int)GameState.Started + 1;
            return View("ListOfGames", Platform.Games.Where(g => g.Game.State == GameState.Started && g.Game.HasUser(User.Identity.Name)).ToList());
        }

        public ActionResult StartedGames()
        {
            ViewBag.GameState = (int)GameState.Started;
            return View("ListOfGames", Platform.Games.Where(g => g.Game.State == GameState.Started).ToList());
        }

        public ActionResult Profile()
        {
            return View(DBContext.GetUserByLogin(User.Identity.Name));
        }

    }
}
