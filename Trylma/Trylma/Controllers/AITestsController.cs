﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trylma.AI;
using Trylma.Helpers;
using Trylma.Models;

namespace Trylma.Controllers
{
    public class AITestsController : Controller
    {

        private class TetsResult
        {
            public string PlayerName;
            public int Moves;
            public bool Winner;
        };

        [HttpPost]
        public JsonResult TestGame(int boardSize, string playersStr, int startBase)
        {
            if (playersStr.Length == 0) return Json(false);

            string[] players = playersStr.Split(' ');

            Dictionary<int, TetsResult> results = new Dictionary<int, TetsResult>();

            //GameRoom room = new GameRoom(DBContext.GetUserByLogin(User.Identity.Name));
            GameRoom room = new GameRoom(new User());
            room.Game.Mode = GameMode.Standard;

            int[][] bases = { 
                                new int[]{ 1 }, 
                                new int[]{ 1, 2 }, 
                                new int[]{ 2, 4, 3 }, 
                                new int[]{ 3, 4, 5, 6}
                            };


            foreach (string p in players)
            {
                int ip;
                if (int.TryParse(p, out ip))
                {
                    if (ip == (int)PlayerType.HeuristicAI)
                    {
                        room.Game.AddPlayer(Trylma.Models.User.HeuristicAIUser);
                    }
                    else
                    {
                        room.Game.AddPlayer(Trylma.Models.User.MinMaxAIUser);
                    }
                    Player pl = room.Game.Players[room.Game.Players.Count - 1];
                    //bases.Add(pl.Id+startBase);
                    results.Add(pl.Id, new TetsResult { PlayerName = pl.Name, Moves = 0 });
                }
                else
                {
                    return Json(false);
                }
            }

            Random r = new Random(DateTime.Now.Millisecond);
            for (int j = 0; j < bases.Length; j++)
            {
                bases[j] = bases[j].OrderBy(b => r.Next()).ToArray();
            }

            int i = 0;
            foreach (var player in room.Game.Players)
            {
                player.SetBases(bases[room.Game.Players.Count-1][i]);
                i++;
            }

            if (players.Count() == 1)
            {
                room.Game.Players[0].SetBases(startBase);
            }

            room.Game.SetBoardSize((BoardSize)boardSize);
            room.Game.Start();

            while (!room.Game.IsGameOver)
            {
                room.Game.NextMove();
                room.Game.NextPlayer();
            }


            foreach (var pl in room.Game.Players)
            {
                results[pl.Id].Moves = pl.Moves;
                results[pl.Id].Winner = pl.Finished;
                //if (pl.Finished) Debug.WriteLine
            }
            //results[room.Game.WinnerId].Winner = true;

            return Json(JsonConvert.SerializeObject(results));
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Test()
        {
            BoardTest();
            return View();
        }

        public void BoardTest() {

            GameRoom room = new GameRoom(DBContext.GetUserByLogin(User.Identity.Name));
            

            room.Game.AddPlayer(Trylma.Models.User.MinMaxAIUser);
            room.Game.AddPlayer(Trylma.Models.User.MinMaxAIUser);

            room.Game.Players[0].SetBases(1);
            room.Game.Players[1].SetBases(3);
            room.Game.SetBoardSize(BoardSize.Medium);

            var v1 = new Vector2D(0, 12);
            var v2 = new Vector2D(18, 6);

            var v11 = GameBoard.FlattenVector(v1, room.Game.Players[0]);
            var v22 = GameBoard.FlattenVector(v2, room.Game.Players[1]);

            Debug.WriteLine(v11.Length());
            Debug.WriteLine(v22.Length());
        }

    }
}
