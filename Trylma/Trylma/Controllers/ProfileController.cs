﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trylma.Helpers;

namespace Trylma.Controllers
{
    public class ProfileController : Controller
    {
        public ActionResult UploadAvatar(HttpPostedFileBase file)
        {
            Models.User us = DBContext.GetUserByLogin(User.Identity.Name);
            if (file != null)
            {
                string pic = System.IO.Path.GetFileName(file.FileName);
                string folder = "/Resources/Images/Avatars";
                string path = System.IO.Path.Combine(Server.MapPath("~"+folder), pic);
                file.SaveAs(path);
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                us.AvatarPath = System.IO.Path.Combine(folder, pic);
                DBContext.SetAvatar(us.Name, us.AvatarPath);
            }
            return RedirectToAction("Profile", "Platform");
        }
    }
}
