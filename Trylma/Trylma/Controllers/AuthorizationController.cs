﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Trylma.Helpers;
using Trylma.Models;

namespace Trylma.Controllers
{
    [AllowAnonymous]
    public class AuthorizationController : Controller
    {
        //
        // GET: /Authorization/
        [HttpGet]
        public ActionResult Login(string url)
        {
            return View(new Credential
            {
                ReturnUrl = url
            });
        }

        [HttpPost]
        public ActionResult Login(Credential credential)
        {
           // if (!ModelState.IsValid) return View();

            if ( ModelState.IsValid && !credential.Login.Equals(Trylma.Models.User.HeuristicAIUser.Name) && !credential.Login.Equals(Trylma.Models.User.MinMaxAIUser.Name) && DBContext.UserExists(credential.Login, credential.Password) && !credential.Login.Equals(Trylma.Models.User.HeuristicAIUser.Name) && !credential.Login.Equals(Trylma.Models.User.MinMaxAIUser.Name))
            {
                var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, credential.Login),
                },
                "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);

                return Redirect(GetRedirectUrl(credential.ReturnUrl));
            }

            ModelState.AddModelError("", "Login lub hasło są nieprawidłowe");
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Credential credential, string note)
        {
            if (credential.Login.Length > 20)
            {
                ModelState.AddModelError("", "Login jest zbyt długi. Maksymalna dopuszczalna długość to 20 znaków.");
                return View();
            }
            if (!ModelState.IsValid) return View();

            if (DBContext.GetUserByLogin(credential.Login) == null && DBContext.AddNewUserToDatabase(credential.Login, credential.Password, note)) 
            {
                // logowanie
                var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, credential.Login),
                },
                "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);

                return Redirect(GetRedirectUrl(credential.ReturnUrl));
            }

            ModelState.AddModelError("", "Użytkownik o takim loginie już istnieje. Wybierz inną nazwę.");
            return View();
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Index", "Platform");
            }

            return returnUrl;
        }

        public ActionResult LogOut()
        {
            Platform.Logout(User.Identity.Name);
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;
            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Index", "Platform");
        }
    }
}
