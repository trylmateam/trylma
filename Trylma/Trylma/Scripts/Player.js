﻿//klasa gracza
function Player(id, index, name, isHuman, pawnsCount) {
    //pionki gracza - kopiowane z tablicy z domyślnymi pozycjami
    this.id = id;
    this.index = index;
    this.name = name;
    this.pawns = [];
    this.correctMoves = [];
    this.isHuman = isHuman;

    for (var i = 0; i < pawnsCount; i++) {
        this.pawns.push(new Pawn(id, index, -1, -1, i));
    }

    this.updatePawns = function (pawns) {
        for (var i = 0; i < this.pawns.length; i++) {
            this.pawns[i].x = pawns[i].X;
            this.pawns[i].y = pawns[i].Y;
            this.pawns[i].id = pawns[i].Id;
        }
    }

    this.getPawnById = function (id) {
        var res = null;
        this.pawns.forEach(function (pawn) {
            if (pawn.id == id) res = pawn;
        });
        return res;
    }

    this.hasPawn = function (p) {
        var res = false;
        this.pawns.forEach(function (pawn) {
            if (pawn.x == p.x && pawn.y == p.y)
                res = true;
        });
        return res;
    }

    this.findPawnReplacement = function (pawns) {
        var movedPawn = null;
        var to = null;
        for (var i = 0; i < pawns.length; i++) {
            var pawn = this.getPawnById(pawns[i].id);

            if (pawn.x != pawns[i].x || pawn.y != pawns[i].y) {
                movedPawn = pawn;
                to = { x: pawns[i].x, y: pawns[i].y };
            }
        }
        return { pawn: movedPawn, to: to };
    }

    //wybrany (podniesiony) pionek
    this.selectedPawnIndex = -1;
};

Player.prototype.getSelectedPawn = function () {
    if (this.selectedPawnIndex >= 0 && this.selectedPawnIndex < this.pawns.length)
        return this.pawns[this.selectedPawnIndex];
    return null;
}

Player.prototype.selectPawn = function (pawn) {
    if (!pawn) {
        this.selectedPawnIndex = -1;
        this.correctMoves = [];
    }
    else {
        this.selectedPawnIndex = this.pawns.indexOfObject(pawn);
    }
}