
function BoardView(board, x, y) {
	var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");
	
	var fieldCircles = [];
	var path = null;
	var hints = null;
	var selectedFields = [];
	var selectedPawns = [];
	
	var boardX = x;
	var boardY = y;
	
	this.hoverField = null;
	this.drawCoords = false;
	
	var getMousePos = function(evt) {
		var rect = canvas.getBoundingClientRect();
		return {
			x: evt.clientX - rect.left,
			y: evt.clientY - rect.top
		};
	}

	this.board = board;
	board.setView(this);
	this.backgroundColor = '#ffffff';
	
	this.onFieldEnter = function(field) {}
	this.onFieldLeave = function(field) {}
	this.onFieldClick = function(field) {}
	this.onMouseMove = function(mousePos) {}
	
	this.prepareDrawData = function () {
	    fieldCircles = [];
		for (var k=0;k<2;k++) {
			var x = boardX;
			for (var i=0;i<Config.board.width;i++) {	
				var startx = x;
				for (var j=0;j<=i;j++) { 
					y = (k == 0 ? i : Config.board.height-1-i)*(Config.board.r*2+Config.board.paddy)+boardY;
					if (fieldCircles.indexOfObject({x: x, y: y}) == -1) {
						fieldCircles.push({x: x, y: y});	
					}
					x += Config.board.r*2+Config.board.paddx;
				}
				x = startx - Config.board.r*2+Config.board.paddx;
			}
		}
	}
	
	this.animateMove = function(pawn, field, speed, onFinish) {
		var pawnPos = this.fieldToPixel(pawn);
		var fieldPos = this.fieldToPixel(field);
		
		var moveVector = new vector2d(fieldPos.x - pawnPos.x, fieldPos.y - pawnPos.y);
		moveVector.normalize();
		
		var pos = this.fieldToPixel(pawn);
		var vectorPos = new vector2d(pos.x, pos.y);
		
		var me = this;
		var animation = setInterval(function()
		{	
			vectorPos = vectorPos.plus(moveVector.mul(speed));
			
			me.clearCanvas();
			me.drawBoard();
			me.drawPawnFree(pawn, vectorPos);
			
			var v = new vector2d(fieldPos.x - vectorPos.x, fieldPos.y - vectorPos.y);
			if (floatEquals(v.cosBetween(moveVector), -1.0, 0.1)) {
				clearInterval(animation);
				onFinish();
			}
		}, 100);
	}
	
	this.drawPawn = function(pawn) {
	    pixel = this.fieldToPixel({ x: pawn.x, y: pawn.y });
	    var color = Config.theme.pawnsColors[pawn.colorId][0];
		this.drawCircle(pixel.x, pixel.y, Config.board.r*0.7, color);
	}
	
	this.drawPawnFree = function (pawn, pixel) {
	    var color = Config.theme.pawnsColors[pawn.colorId][0];
		this.drawCircle(pixel.x, pixel.y, Config.board.r*0.7, color);
	}
	
	this.highlightPawn = function (pawn) {
	    if (pawn == null) selectedPawns = [];
	    else {
	        var color = Config.theme.pawnsColors[pawn.colorId][1];
	        selectedPawns.push({ pawn: pawn, color: color });
	    }
	}

	this.drawPath = function (p) {
	    path = p;
	}

	this.drawHints = function (h) {
	    hints = h;
	}
	
	this.drawBoard = function() {
		var me = this;

		fieldCircles.forEach(function (c) {
			me.drawCircle(c.x, c.y, Config.board.r, Config.theme.fieldColor);
		});

		selectedFields.forEach(function (el) {
		    coord = me.fieldToPixel(el.field);
		    me.drawCircle(coord.x, coord.y, Config.board.r, el.color);
		});

		if (path) {
		    path.forEach(function (f) {
		        coord = me.fieldToPixel(f);
		        me.drawCircle(coord.x, coord.y, Config.board.r, 'Silver');
		    });
		}

		if (hints) {
		    hints.forEach(function (f) {
		        coord = me.fieldToPixel(f);
		        me.drawCircle(coord.x, coord.y, Config.board.r, 'LawnGreen');
		    });
		}

		this.board.getPawns().forEach(function(pawn) {
			me.drawPawn(pawn);
		});

		selectedPawns.forEach(function(el) {
		    coord = me.fieldToPixel(el.pawn.getField());
		    me.drawCircle(coord.x, coord.y, Config.board.r*0.7, el.color);
		});
	}
	
	this.redrawBoard = function() {
		this.clearCanvas();
		this.drawBoard();
	}

	this.clearPath = function() {
	    path = [];
	}

	this.clearHints = function() {
	    hints = [];
	}

	this.clearSelectedPawns = function() {
	    selectedPawns = [];
	}
	
	this.clearField = function(field) {
		coord = this.fieldToPixel(field);
		ctx.fillStyle = Config.board.backgroundColor;
		ctx.fillRect(coord.x-Config.board.r-1,coord.y-Config.board.r-1,Config.board.r*2+2, Config.board.r*2+2);
	}
	
	this.clearCanvas = function() {
		ctx.fillStyle = Config.theme.backgroundColor;
		ctx.fillRect(0, 0, canvas.width, canvas.height);
	}
	
	this.selectFields = function(fields, color) {
		for (var i=0;i<fields.length;i++){
			this.selectField(fields[i], color);
		}
	}

	this.selectField = function (field, color) {
	    selectedFields.push({ field: field, color: color });
	}
	
	this.pixelToField = function(pixel) {	
		for (var i=0;i<fieldCircles.length;i++) {
			var c = fieldCircles[i];
			if (Math.sqrt(Math.pow(pixel.x-c.x, 2) + Math.pow(pixel.y-c.y, 2)) <= Config.board.r) {
				field = board.getFields()[i];
				return new Field(field);
			}
		}
		return null;
	}
	
	this.fieldToPixel = function (field) {
		var fields = board.getFields();
		for (var i=0;i<fields.length;i++) {
			var f = fields[i];
			if (f.x === field.x && f.y === field.y) {
				return fieldCircles[i];
			}
		}
		return null;
	}
	
	this.drawCircle = function(x, y, r, color) {
		ctx.beginPath();
		ctx.arc(x, y, r, 0, 2*Math.PI);
		ctx.fillStyle = color;
		ctx.fill();
		ctx.stroke();
		
		if (this.drawCoords) {
			var coord = this.pixelToField({x: x, y: y});
			if (coord) {
				ctx.fillStyle = "black";
				ctx.font = "bold 10px Arial";
				ctx.fillText(coord.x+', '+coord.y, x-Config.board.r*0.5-6, y+3);
			}
		}
	}
	
	this.mouseMoveFun = function(event) {
		mousePos = getMousePos(event);
		var prevField = this.hoverField;
		
		this.hoverField = this.pixelToField({x: mousePos.x, y: mousePos.y });
		
		//generacja zdarzenia FieldLeave
		if (prevField) {
			if (!this.hoverField || !prevField.is(this.hoverField)) {
				this.onFieldLeave(prevField);
			}
		}
		
		//generacja zdarzenia FieldEnter
		if (this.hoverField) {
			if (!prevField || !prevField.is(this.hoverField)) {
				this.onFieldEnter(this.hoverField);
			}
		}
		
		//generacja zdarzenia MouseMove
		this.onMouseMove(mousePos);
	   
        /*
		var p = document.getElementById("output");
		while (p.firstChild) {
			p.removeChild(p.firstChild);
		}*/

		if (this.hoverField) {
			//p.appendChild(document.createTextNode("selectedField x: "+this.hoverField.x+" y: "+this.hoverField.y));
			//p.appendChild(document.createElement('br'));
			/*
			var pawn = board.getPawnFromField(this.hoverField);
			if (pawn) {
				p.appendChild(document.createTextNode("pawn player: "+pawn.playerId));
				p.appendChild(document.createElement('br'));
				p.appendChild(document.createTextNode("pawn id: "+pawn.id));
			}*/
		}
	}
	
	this.mouseDownFun = function(event) {
		mousePos = getMousePos(event);
		var selectedField = this.pixelToField({x: mousePos.x, y: mousePos.y });
		
		if (selectedField) {
			this.onFieldClick(selectedField);
		}
	}
	
	this.init = function() {
		var me = this;
		canvas.addEventListener('mousemove', function(e) { me.mouseMoveFun(e); }, false);
		canvas.addEventListener('mousedown', function(e) { me.mouseDownFun(e); }, false);
		
		this.prepareDrawData();
	}
}