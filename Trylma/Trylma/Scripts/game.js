﻿
//klasa gry
function Game() {
    this.turnCounter = 0;

    this.boardSize = {
        SMALL: 0,
        MEDIUM: 1,
        LARGE: 2
    };

    this.playerType = {
        HUMAN: 0,
        HEURISTIC: 1,
        MINMAX: 2
    };
	
	var me = this;
	var board = new Board();
	var boardView = new BoardView(board, 300, 20);
	var gameId;
	var players = [];
	var currentPlayerId;
	var animation = false;

	this.endedMove = 0;

	this.getId = function () {
	    return gameId;
	}

	this.onChangePlayer = function () {
	    $('div .currentPlayer').removeClass('currentPlayer');
	    $('div #' + currentPlayerId).addClass('currentPlayer');
        /*
        var cp = document.getElementById('currentPlayer');
        while (cp.firstChild) {
            cp.removeChild(cp.firstChild);
        }
        cp.appendChild(document.createTextNode(this.getCurrentPlayer().name+" Runda:"+this.turnCounter));*/
	    };
	
	this.getBoardView = function() {
		return boardView;
	}
	
	this.getPlayer = function (index) {
	    if (index >= players.length) return null;
		else return players[index];
	}

	this.getPlayerById = function (id) {
	    var player = null;
	    players.forEach(function (pl) {
	        if (pl.id == id) {
	            player = pl;
	        }
	    });
	    return player;
	}

	this.getPlayerByName = function(name) {
	    var player = null;
	    players.forEach(function(pl) {
	        if (pl.name == name) {
	            player = pl;
	        }
	    });
	    return player;
	}
	
	this.getCurrentPlayer = function () {
	    return this.getPlayerById(currentPlayerId);
	}
	
	this.getPlayers = function() {
		return players;
	}

	this.synchronizeBoard = function (playersDescs, pawns) {

	    players = [];
	    Config.board.playerIds = [];
	    Config.board.playerNames = [];
	    Config.board.playerUsers = [];

	    playersDescs.forEach(function (desc) {
	        Config.board.playerNames[desc.Id] = desc.Name;
	        Config.board.playerIds.push(desc.Id);
	        Config.board.playerUsers[desc.Id] = desc.UserName;
	        players.push(new Player(desc.Id, desc.Index, desc.Name, desc.Type == 0, Config.board.pawnsCount));
	    });

	    var pawnsByPlayer = [];
	    pawns.forEach(function (pawn) {
	        //var name = Config.board.playerNames[pawn.Owner];
	        var name = pawn.Owner;
	        if (!pawnsByPlayer.hasOwnProperty(name)) {
	            pawnsByPlayer.push(name);
	            pawnsByPlayer[name] = new Array();
	        }
	        pawnsByPlayer[name].push(pawn);
	    });

	    Config.board.playerIds.forEach(function (playerId) {
	        me.getPlayerById(playerId).updatePawns(pawnsByPlayer[playerId]);
	    });

	    board.removePawns();

	    players.forEach(function (player) {
	        player.pawns.forEach(function (pawn) {
	            board.addPawn(pawn);
	        });
	    });
	}

	this.draw = function () {
	    boardView.drawBoard();
	}

	this.animatedMove = function (move) {
	    var pawn = players[move.Pawn.Owner].getPawnById(move.Pawn.Id);
	    var field = { x: move.To.X, y: move.To.Y };
	    var path = board.getMovePath(pawn, field);
	    animation = true;
	    me.artificalMoveSequence(pawn, path, 1, function () {
	        animation = false;
	        me.clearCurrentPlayer();
	        me.onMoveEnd(gameId);
	    });
	}

	this.artificalMoveSequence = function (pawn, path, i, finishFun) {
	   
	    boardView.drawPath(path);
	    
	    if (path == null) return;
	    if (i < path.length) {
	        this.artificalMove(pawn, path[i], function () {
	            me.artificalMoveSequence(pawn, path, ++i, finishFun);
	        });
	    }
	    else {
	        finishFun();
	    }
	}

	this.artificalMove = function (pawn, field, onEnd) {
	    if (!pawn || !field) return;
	    board.removePawn(pawn);
	    boardView.animateMove(pawn, field, Config.board.animationSpeed, function () {
	        pawn.move(field);
	        board.addPawn(pawn);
	        boardView.redrawBoard();
	        onEnd();
	    });
	}

	this.setCurrentPlayer = function (playerId) {
	    currentPlayerId = playerId;
	}

	this.setRound = function (round) {
	    this.turnCounter = round;
	}

	this.getCurrentPlayerUserName = function () {
	    Config.board.playerUsers[currentPlayerId];
	}
	
	boardView.onFieldEnter = function (f) {
	    if (animation) return;
		var pawn = board.getPawnFromField(f);
		if (pawn && pawn.playerId === currentPlayerId && me.getCurrentPlayer().isHuman && Config.board.playerUsers[currentPlayerId] == Config.user) {
		    if (!me.getCurrentPlayer().getSelectedPawn()) 
		    {
		        me.getCurrentPlayer().correctMoves = board.getCorrectMoves(pawn).moves;
				boardView.drawHints(me.getCurrentPlayer().correctMoves);
				boardView.highlightPawn(pawn);
				boardView.drawBoard();
			}
		}
	}
	
	boardView.onFieldLeave = function (f) {
	    if (animation) return;
	    
	    if (!me.getCurrentPlayer().getSelectedPawn()) {
	        me.getCurrentPlayer().correctMoves = [];
	        boardView.clearHints();
	        boardView.clearSelectedPawns();
	        boardView.redrawBoard();
		}
	}


	boardView.onFieldClick = function (f) {
	    if (animation) return;
	    boardView.clearHints();
	    boardView.clearSelectedPawns();

	    var pawn = board.getPawnFromField(f);
		
	    //wzięcie pionka
	    var selectedPawn = me.getCurrentPlayer().getSelectedPawn();
	    if (!selectedPawn && pawn && pawn.playerId === currentPlayerId && Config.board.playerUsers[currentPlayerId] == Config.user) {
	        me.getCurrentPlayer().selectPawn(pawn);
	        me.getCurrentPlayer().correctMoves = board.getCorrectMoves(pawn).moves;
	    }
	    else if (selectedPawn && !pawn)
	    {
	        if (me.getCurrentPlayer().correctMoves.indexOfObject(f) == -1) {
	            me.getCurrentPlayer().selectPawn(null);
	            me.getCurrentPlayer().correctMoves = [];
	        }
	        else {
	            me.tryMovePawn(me.getCurrentPlayer().getSelectedPawn().getField(), f, gameId);
	            me.getCurrentPlayer().selectPawn(null);
	        }
	    }

	    boardView.highlightPawn(me.getCurrentPlayer().getSelectedPawn());
	    boardView.drawHints(me.getCurrentPlayer().correctMoves);

		boardView.drawBoard();
	}

	this.clearCurrentPlayer = function () {
	    currentPlayerId = -1;
	    $('div .currentPlayer').removeClass('currentPlayer');
	}
	
	this.init = function (desc) {

	    Config.playerNames = [];
	    players = [];
	    gameId = desc.GameId;

	    Config.board.height = desc.BoardHeight;
	    Config.board.width = desc.BoardWidth;
	    Config.board.pawnsCount = desc.PawnsNum;
	    currentPlayerId = desc.CurrentPlayer;

	    this.synchronizeBoard(desc.PlayersDescs, desc.Pawns);
		board.init();
		this.getBoardView().init();
		
		boardView.redrawBoard();
		this.onChangePlayer();
	}
}