//dodatkowa metoda do standardowej klasy Array, pozwalająca na wyszukiwanie indeksy obiektów
Array.prototype.indexOfObject = function (object) {
	var i = 0;
	
	for (var i=0;i<this.length;i++) {
		var equal = true;
		
		var keys = Object.keys(object);
		for (var j=0;j<keys.length;j++) {
			if (this[i][keys[j]] != object[keys[j]]) { 
				equal = false;
				break;
			}
		}
		if (equal) return i;
	}
	return -1;
}


function floatEquals(f1, f2, precision) {
	return Math.abs(f1-f2) < precision;
}

function vector2d(x, y) {
	this.x = x;
	this.y = y;
}

vector2d.prototype.length = function() {
	return Math.sqrt(this.x*this.x+this.y*this.y);
}

vector2d.prototype.normalize = function() {
	var len = Math.sqrt(this.x*this.x+this.y*this.y);
	this.x /= len;
	this.y /= len;
}

vector2d.prototype.mul = function(f) {
	return new vector2d(this.x*f, this.y*f);
}

vector2d.prototype.plus = function(v) {
	return new vector2d(this.x+v.x, this.y+v.y);
}

vector2d.prototype.neg = function(v) {
	return new vector2d(-this.x, -this.y);
}

vector2d.prototype.dotProduct = function(v) {
	return this.x*v.x + this.y*v.y;
}

vector2d.prototype.cosBetween = function(v) {
	return this.dotProduct(v) / (this.length()*v.length());
}



