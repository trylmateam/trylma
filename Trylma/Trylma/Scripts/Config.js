
function BoardConfig(w, h, r, padx, pady) {
	this.width = w;
	this.height = h;
	this.r = r;
	this.paddx = padx;
	this.paddy = pady;
	this.playerNames = [];
	this.playerIds = [];
	this.playerUsers = [];
	this.startPlayer = 'Red';
	this.pawnsCount = 10;
	this.animationSpeed = 20;
	this.user = "";

	this.getIndexByName = function (name) {
	    var k = 0;
	    for (var i = 0; i < this.playerNames.length; i++) {
	        if (this.playerNames[i] != undefined) {
	            if (this.playerNames[i] == name)
	                return k;
	            k++;
	        }
	    }
	    return -1;
	}
};


function Theme(backgroundC, fieldC, validMoveC, invalidMoveC, pawnsColors) {
	this.backgroundColor = backgroundC;
	this.fieldColor = fieldC;
	this.validMoveColor = validMoveC;
	this.invalidMoveC = invalidMoveC;
	this.pawnsColors = pawnsColors;
}

var defaultConfig = new BoardConfig(13, 17, 15, 10, 5);
var defaultTheme = new Theme('white', 'yellow', 'Chartreuse', 'DarkRed', 
								[
									['red', 'DarkSalmon'],
									['green', 'GreenYellow '],
									['blue', 'MediumTurquoise '],
									['DarkOrange ', 'Khaki'],
									['black', 'gray'],
									['white', 'silver'],
								]
							);

function Config() {}
Config.board = defaultConfig;
Config.theme = defaultTheme;