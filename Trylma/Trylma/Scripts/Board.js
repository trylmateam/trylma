Board.startPositions = [];

//klasa pionka
function Pawn(playerId, colorid, x, y, id) {
    this.x = x;
	this.y = y;
	this.playerId = playerId;
	this.id = id;
	this.colorId = colorid;
}
//czy jest na danym polu
Pawn.prototype.on = function(field) {
	return this.x === field.x && this.y === field.y;
};

Pawn.prototype.move = function(field) {
	this.x = field.x;
	this.y = field.y;
};

Pawn.prototype.getField = function() {
	return new Field({ x: this.x, y: this.y });
}

//klasa pola
function Field(coords) {
	this.x = coords.x;
	this.y = coords.y;
}

Field.compareY = function(f1, f2) {
	if (f1.y < f2.y) return 1;
	else if (f1.y > f2.y) return -1;
	else return 0;
}
				
Field.compareX = function(f1, f2) {
	if (f1.x < f2.x) return 1;
	else if (f1.x > f2.x) return -1;
	else return 0;
}

Field.prototype.has = function(pawn) {
	return this.x === pawn.x && this.y === pawn.y;
};

Field.prototype.inOddRow = function() {
	return !(this.y % 2 == 0);
}

Field.prototype.inEvenRow = function() {
	return this.y % 2 == 0;
}

Field.prototype.is = function(f) {
	return this.x == f.x && this.y == f.y;
}

//Klasa planszy
function Board() {
	var view;
	var pawns = [];
	var fields = [];
	
	this.getFields = function() {
		return fields;
	}
			
	this.setView = function(v) {
		view = v;
	};

	this.removePawns = function () {
	    pawns = [];
	}
	
	this.addPawn = function(pawn) {
		pawns.push(pawn);
	}
	
	this.removePawn = function(pawn) {
		var index = pawns.indexOfObject(pawn);
		if (index >= 0) pawns.splice(index, 1);
	}
	
	this.getPawns = function() {
		return pawns;
	}
	
	this.getPawnFromField = function(field) {
		if (!field) return null;
		for (var i=0;i<pawns.length;i++) {
			if (field.has(pawns[i])) return pawns[i];
		}
		return null;
	};
		
	//zwraca true jeśli pole leży w granicach planszy
	this.inBoundaries = function(field){	
		return fields.indexOfObject(field) != -1;
	}
	
	this.pointCircleTest = function(px, py, cx, cy, r){
		return (px > cx-r && px < cx+r && py > cy-r && py < cy+r );
	}

	this.init = function () {
	    fields = [];
		var startX = Config.board.width-1;
		for (var k=0;k<2;k++) {
			for (var i=0;i<Config.board.width;i++) {
				var x = startX - i;
				for (var j=0;j<=i;j++) { 
					var field = {
						x: x, 
						y: (k == 0 ? i : Config.board.height-1-i)
					};
					if (fields.indexOfObject(field) == -1) 
						fields.push(field);
					x+=2;
				}
			}
		}
		
		for (var p=0;p<2;p++) {
			Board.startPositions[p] = new Array();
			for (var i=0;i<10;i++) {
				Board.startPositions[p].push(fields[p == 0 ? i : i+Config.board.width*(Config.board.width+1)/2]);
			}
			Board.startPositions[p]['player'] = p;
		}
	}
	
	
	//zwraca wszystkie pola leżące w tej samej osi. oznaczenia osi: (0 / 1 \ 2 -)
	this.getAxisFields = function(field, axis) {
		var fields = [ field ];
		var forwardField = field;
		var backwardField = field;
		var axisMatrixX = [ [-1, 1], [1, -1], [-2, 2]];
		var axisMatrixY = [ [-1, 1], [-1, 1], [0, 0]];
		
		while (true) {
			
			if (forwardField) forwardField = new Field({x: forwardField.x+axisMatrixX[axis][0], y: forwardField.y+axisMatrixY[axis][0] });
			if (backwardField) backwardField = new Field({x: backwardField.x+axisMatrixX[axis][1], y: backwardField.y+axisMatrixY[axis][1]});
			
			if (forwardField && this.inBoundaries(forwardField)) fields.push(forwardField);
			else forwardField = null;
			
			if (backwardField && this.inBoundaries(backwardField)) fields.push(backwardField);
			else backwardField = null;
			
			if (!forwardField && !backwardField) break;
		}
		
		return fields;
	}
	
	this.getNeighbours = function(field) {
		var n = [	new Field({ x: field.x-2, y: field.y}), 
					new Field({ x: field.x+2, y: field.y}), 
					new Field({ x: field.x-1, y: field.y-1}), 
					new Field({ x: field.x+1, y: field.y-1}),
					new Field({ x: field.x-1, y: field.y+1}),
					new Field({ x: field.x+1, y: field.y+1})
				];
				
		
		for (var i=n.length-1;i>=0;i--) {
			if (!this.inBoundaries(n[i])){
				n.splice(i, 1);
			}
		}
		return n;
	}

	this.getCorrectPaths = function (pawn) {

	}
	
	//funkcja zwracająca tablicę z dozwolonymi ruchami (wszystkimi, z uwzględnieniem skakania). w podstawowej wersji mozna skakać tylko o 1 pole, w rozszerzonej o dowolną ilość (ale w środku musi stać tylko jeden pionek)
	this.getCorrectMoves = function(pawn) {
		
		var neighbours = this.getNeighbours(pawn.getField());
		var correctMoves = [ pawn.getField() ];
		var correctPaths = [ ];
		
		var me = this;
		recursiveGetCorrectMoves = function (field, path) {

            //dodajemy nową ścieżkę do ścieżek
		    var currentPath = path.slice();
		    currentPath.push(field);
		    correctPaths.push(currentPath);

			for (var axis=0;axis<3;axis++) 
			{
				var axisFields = me.getAxisFields(field, axis);
				
				axisFields.sort(axis == 2 ? Field.compareX : Field.compareY);
				var indexOfCurrentField = axisFields.indexOfObject(field);
				
				for (var j=0;j<2;j++) {
					var pawnFound = false;
					var indexOfFoundPawn = 0;
					for (var i=indexOfCurrentField;(j === 0 ? i<axisFields.length : i >= 0);(j === 0 ? i++ : i--)) {
						if (i === indexOfCurrentField) continue;
						if (!pawnFound) {
							if (me.getPawnFromField(axisFields[i])) {
								pawnFound = true;
								indexOfFoundPawn = i;
							}
						}
						else {
							if (me.getPawnFromField(axisFields[i])) break;
							if ((indexOfFoundPawn - indexOfCurrentField) === (i - indexOfFoundPawn) && Math.abs(i - indexOfFoundPawn) === 1) {		//usun Math.abs(i - indexOfFoundPawn) === 1 zeby miec wersję rozszerzoną
								if (correctMoves.indexOfObject(axisFields[i]) == -1) {
								    correctMoves.push(axisFields[i]);
								    recursiveGetCorrectMoves(axisFields[i], currentPath.slice());
								}
								break;
							}
						}
					}
				}
			}
		}
		
		//pobieramy poprawne pola wynikające z mozliwośći przeskakiwania
		recursiveGetCorrectMoves(pawn.getField(), []);
		
		//dla każdego pola sąsiedniego
		neighbours.forEach(function (n) {
		    if (!me.getPawnFromField(n)) {
		        correctPaths.push([pawn.getField(), n]);
		        if (correctMoves.indexOfObject(n) === -1)
				    correctMoves.push(n);
			}
		});
		
		correctMoves.push(pawn.getField());
		
		return {
		    moves: correctMoves,
		    paths: correctPaths
		};
	}

	this.getMovePath = function (pawn, field) {
	    var paths = this.getCorrectMoves(pawn).paths;
	    var choosenPath = null;
	    paths.forEach(function (p) {
	        var lastField = p[p.length - 1];
	        if (lastField.x == field.x && lastField.y == field.y) {
                if (choosenPath == null || p.length < choosenPath.length)
	                choosenPath = p;
	        }
	    });
	    return choosenPath;
	}
}