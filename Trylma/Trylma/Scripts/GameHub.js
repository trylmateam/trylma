﻿$(function () {
    var gameHub = $.connection.gameHub;


    //zdarzenia związane z grą
    //-----------------------------------
    gameHub.client.refreshBoard = function (gameId, players, pawns, move) {
        if (gameId == game.getId()) {
            console.log('------------ board synchronization (refreshBoard)');
            console.log('------------ move = '+move);
            game.synchronizeBoard(players, pawns);
            game.endedMove = move;
            game.draw();
        }
    };

    gameHub.client.beginMove = function (gameId, playerId, round, move) {
        if (gameId == game.getId()) {
            
            console.log("--------------- beginMove player: " + playerId + " endedMove: " + game.endedMove + " round: " + round + " move: " + move);
            game.setCurrentPlayer(playerId);
            game.setRound(round);
            game.onChangePlayer();

            if (game.endedMove + 1 < move) {
                //console.log('!!-------------  i need synchronization! sending sync request...');
                //gameHub.server.synchronizeRequest(gameId);
            }
        }
    };

    gameHub.client.move = function (gameId, move) {
        if (gameId == game.getId()) {
            game.endedMove++;
            console.log("received move "+game.endedMove);
            game.animatedMove(move);
            console.log(move);
        }
    }

    gameHub.client.gameOver = function (gameId, playerId) {
        if (gameId == game.getId()) {
            alert('Wygrał gracz ' + game.getPlayer(playerId).name + '.');
        }
    }

    //notyfikacje od serwera że zmienił sie stan i trzeba zgłosić się po nowy element
    //------------------------------------------

    gameHub.client.onRoomChange = function (gameId, newplid, newuid) {
        if (game.getId() == gameId) {
            $.post("/GameRoom/Update", {
                gameId: gameId,
                newPlayerId: newplid,
                newUserId: newuid
            });
        }
    }

    gameHub.client.onGameOver = function (gameId) {
        if (game.getId() == gameId) {
            $.post("/GameRoom/GetGameOverInfo", {
                gameId: gameId,
            }, function (result) {
                if (result != false) {
                    $('#headarea').append(result);
                    var el = $('#gameoverinfo');
                    el.focus();
                    el.removeClass('new');
                }
            });
        }
    }

    gameHub.client.onStartGame = function (gameId) {
        if (game.getId() == gameId) {
            console.log('onStartGame');
            $.post("/GameRoom/GetGame", {
                gameId: gameId
            }, function (result) {
                var start = setTimeout(function () {
                    gameHub.server.started(gameId);
                }, 1000);
            });
            
        }
    }

    //widoki wstawiane dynamicznie do strony
    //------------------------------------------
    gameHub.client.onMessage = function (gameId, msgHtml) {
        if (gameId == game.getId()) {
            $('.chatarea').prepend(msgHtml);
            var newel = $('.chatarea .msg').first();
            newel.addClass('new');
            newel.focus();
            newel.removeClass('new');
        }
    }

    gameHub.client.updateRoom = function (gameId, userHtml) {
        console.log('UpdateRoom');
        if (gameId == game.getId()) {
            $('.roomtablearea').html(userHtml);
            var o = $('.new');
            o.focus();
            o.removeClass('new');
        }
    }

    gameHub.client.gameArea = function (gameId, userHtml, username, gamedesc) {
        if (gameId == game.getId()) {
            console.log('gameArea');
            $('.roomtablearea').html(userHtml);
            var o = $('.new');
            o.focus();
            o.removeClass('new');
            Config.user = username;
            game.init(gamedesc);
        }
    }

    // Start the connection.
    $.connection.hub.start().done(function () {

        //klient informuje że zakonczył animację ruchu
        game.onMoveEnd = function (gameId) {
            console.log("endMove "+game.endedMove);
            gameHub.server.endMoveRequest(gameId);
        };

        //próba przeniesienia pionka
        game.tryMovePawn = function (from, to, gameId) {
            console.log("tryMovePawn");
            gameHub.server.movePawnRequest(from, to, gameId);
        };

        game.endMove = function () {
            gameHub.server.endMoveRequest(game.getId());
        }

        

        SignalRCompleted();
    });
});