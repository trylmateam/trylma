﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Trylma.Helpers;
using Trylma.Models;

namespace Trylma.AI
{
    public class GameMoveEvaluator
    {
        static private double FatalMove(GameMoveNode move)
        {
            return -10;
        }

        //ocenia ruch na podstawie przybliżenia do bazy
        static public double DistanceEvaluator(GameMoveNode move, Base gameBase, Player pl)
        {
            //nie można wychodzić z bazy
            if (move.LeaveBase()) return FatalMove(move);

            //ruch w bazie dozwolony tylko do przodu
            if (move.InsideBase())
            {
                if (move.To.BaseOrder >= move.From.BaseOrder) return FatalMove(move);
            }

            Field f = gameBase.GetNearestFieldInBaseNotOccupiedBy(move.Pawn);
            if (f == null) return 0.0;

            //aktualny wektor do bazy
            Vector2D toBase = f.GetPos() - move.From.GetPos();
            toBase = GameBoard.FlattenVector(toBase, pl);
            
            //wektor do bazy po wykonaniu ocenianego ruchu
            Vector2D predictedToBase = f.GetPos() - move.To.GetPos();
            predictedToBase = GameBoard.FlattenVector(predictedToBase, pl);
            

            //ruchy oddalające od bazy niedozwolone
            if (predictedToBase.Length() < toBase.Length())
            {
                double toBaseDiff = toBase.Length() - predictedToBase.Length();
                return toBaseDiff * (move.EnterBase() ? 10.0 : 1.0);
            }
            else return FatalMove(move);
        }

        //ocenia pionek na podstawie oddalenia od bazy
        static public double PawnEvaluator(Pawn pawn, Base gameBase, Player pl)
        {
            Field f = gameBase.GetNearestFieldInBaseNotOccupiedBy(pawn);
            if (f == null) return 0.0;
            Vector2D toBase = new Vector2D(f.X - pawn.X, f.Y - pawn.Y);
            toBase = GameBoard.FlattenVector(toBase, pl);
            return toBase.Length();
        }

        static public double ComplexEvaluator(GameMoveNode move, Base gameBase, Player pl)
        {
            Random rand = new Random();
            if (rand.NextDouble() < 0.5)
            {
                return 0.6*PawnEvaluator(move.Pawn, gameBase, pl) + 0.4*DistanceEvaluator(move, gameBase, pl);
            }
            else
            {
                return DistanceEvaluator(move, gameBase, pl);
            }
        }
    }
}