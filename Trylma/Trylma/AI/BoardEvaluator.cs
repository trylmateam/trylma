﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Trylma.Helpers;
using Trylma.Models;

namespace Trylma.AI
{
    public class BoardEvaluator
    {
        //ocenia planszę na podstawie środków ciężkości pionków
        static public double CenterWeightEvaluator(GameBoard board, Player player)
        {
            //liczy środek ciężkości pionków
            Vector2D center = new Vector2D(0.0, 0.0);
            foreach (Pawn p in board.Pawns.Where(x => x.Owner == player.Id))
            {
                center.x += p.X;
                center.y += p.Y;
            }
            center.x /= board.NumberOfPawnsPerPlayer;
            center.y /= board.NumberOfPawnsPerPlayer;
            //---------------------------------------

            Vector2D bcw = player.GetEndBase().MassCenter; //środek ciężkości bazy
            Vector2D dist = new Vector2D(bcw.x - center.x, bcw.y - center.y); //odległość między środkami ciężkości
            
            dist = GameBoard.FlattenVector(dist, player);

            return -(dist.Length());
        }
    }
}