﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Trylma.Models;

namespace Trylma.AI
{
    public interface IArtificallIntelligence
    {
        GameMoveNode Move(Player player);
    }
}
