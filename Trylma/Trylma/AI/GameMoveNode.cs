﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Trylma.Helpers;
using Trylma.Models;

namespace Trylma.AI
{
    public class GameMoveNode : GameMove
    {
        public int Depth;
        public double Rating;
        public double DistanceRating;
        public GameMoveNode PrevMove;
        public List<GameMoveNode> Childs;

        public GameMoveNode()
        {
            Childs = new List<GameMoveNode>();
        }

        public GameMoveNode(Pawn p, Field f, Field t, GameMoveNode gm = null) : base(p, f, t)
        {
            PrevMove = gm;
            Childs = new List<GameMoveNode>();
            Rating = 0.0;
        }

        public int GetNodesCount()
        {
            if (Childs.Count == 0) return 1;
            else return Childs.Sum(c => c.GetNodesCount());
        }

        public int GetLeavesCount()
        {
            if (Childs.Count == 0) return 1;
            else return Childs.Sum(c => c.GetLeavesCount());
        }
    }
}