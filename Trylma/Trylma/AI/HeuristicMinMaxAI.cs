﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Trylma.Helpers;
using Trylma.Models;

namespace Trylma.AI
{
    public class HeuristicMinMaxAI : IArtificallIntelligence
    {
        private enum NodesBuildingResult { Builded, Prunned, PlayerFinished };

        private class NodeValue {
            public double Value;
            public int Depth;
        }

        private int maxDepth = 4;
        private Game Game;
        private GameBoard Board;
        private GameMoveNode Root = new GameMoveNode();
        private Player ControlledPlayer;
        private int NumberOfPlayers;

        private const double epsilon = 0.000001;

        public HeuristicMinMaxAI(int numberOfPlayers, Game game)
        {
            NumberOfPlayers = numberOfPlayers;
            Game = game;
        }

        private NodeValue MinMaxAlphaBetaPrunning(GameMoveNode node, double alpha, double beta, int playerIndex, int depth)
        {
            double v = double.NegativeInfinity;
            int d = depth;

            if (node.Pawn != null) node.Pawn.Move(node.To);
            if (depth < maxDepth)
            {
                if (playerIndex == ControlledPlayer.Index)
                {
                    v = alpha;
                    BuildChildNodes(node, playerIndex, (move) =>
                        {
                            NodeValue value = MinMaxAlphaBetaPrunning(move, v, beta, (playerIndex + 1) % Game.Players.Count, depth + 1);
                            move.Depth = d = value.Depth;
                            move.Rating = v = Math.Max(v, value.Value);
                            return beta <= v;
                        });
                }
                else
                {
                    v = beta;
                    BuildChildNodes(node, playerIndex, (move) =>
                        {
                            NodeValue value = MinMaxAlphaBetaPrunning(move, alpha, v, (playerIndex + 1) % Game.Players.Count, depth + 1);
                            move.Depth = d = value.Depth;
                            move.Rating = v = Math.Min(v, value.Value);
                            return v <= alpha;
                        });           
                }
            }

            if (double.IsInfinity(v) == true)
            {
                v = node.Rating = BoardEvaluator.CenterWeightEvaluator(Board, ControlledPlayer);
                d = depth;
            }

            if (node.Pawn != null) node.Pawn.Move(node.From);

            return new NodeValue { 
                Depth = d, 
                Value = v 
            };
        }

        bool IsEqual(double a, double b, double epsilon)
        {
            if (double.IsNegativeInfinity(a) && double.IsNegativeInfinity(b)) return true;
            if (double.IsPositiveInfinity(a) && double.IsPositiveInfinity(b)) return true;
            return Math.Abs(a - b) <= epsilon;
        }

        public GameMoveNode Move(Player player)
        {
            maxDepth = Math.Max(3, Math.Min(Game.Players.Count + 1, 4));

            ControlledPlayer = player;

            Root.Childs.Clear();
            BoardFactory boardFactory = new BoardFactory();
            Board = boardFactory.CreateBoard(Game.GameBoard);
            NodeValue value = MinMaxAlphaBetaPrunning(Root, Double.NegativeInfinity, Double.PositiveInfinity, player.Index, 0);
            Root.Rating = value.Value;
            Root.Depth = value.Depth;
            //Debug.WriteLine(Root.GetNodesCount());

            /*
            GameMoveNode best = Root.Childs
                .OrderByDescending(m => m.Rating)
                .ThenByDescending(m => m.DistanceRating)
                .FirstOrDefault();*/

                
            GameMoveNode best = Root.Childs.Where(x => IsEqual(x.Rating, Root.Rating, epsilon)).FirstOrDefault();

            if (best != null)
            {
                ControlledPlayer.MovePawn(Game.GameBoard.GetPawnByPlayerAndIndex(ControlledPlayer.Id, best.Pawn.Id), best.To.X, best.To.Y);
            }
            else
            {
                Debug.WriteLine("Minmax nie znalazł ruchu!");
                Game.IsGameOver = true;
            }

            return best;
        }


        bool BuildChildNodes(GameMoveNode node, int playerIndex, Func<GameMoveNode, bool> pruneCondition)
        {
            if (Game.Players[playerIndex].Finished)
            {
                return false;
            }

            List<Pawn> pawns = Board.Pawns.Where(pawn => pawn.Owner ==  Game.Players[playerIndex].Id).ToList();
            pawns = GameBoard.SortPawns(pawns, Game.Players[playerIndex].StartBase-1);
            foreach (var pawn in pawns)
            {
                List<Field> fields = pawn.GetAvailableFields();
                fields = GameBoard.SortFields(fields, Game.Players[playerIndex].StartBase - 1);
                foreach (var field in fields)
                {
                    GameMoveNode move = new GameMoveNode(pawn, pawn.GetOcuppiedField(), field, node);
                    Player pl = Game.Players[playerIndex];
                    move.DistanceRating = GameMoveEvaluator.DistanceEvaluator(move, Board.Bases[pl.EndBase-1], pl);
                    if (move.DistanceRating < epsilon) continue;

                    node.Childs.Add(move);
                    if (pruneCondition(move))
                    {
                        return true;
                    }
                }
            }

            if (node.Childs.Count == 0) return false;
            return true;
        }
    }
}