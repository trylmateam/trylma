﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Trylma.Helpers;
using Trylma.Models;

namespace Trylma.AI
{
    public class HeuristicAI : IArtificallIntelligence
    {
        Player ControlledPlayer;

        public GameMoveNode Move(Player player)
        {
            if (player.Finished) return null;

            ControlledPlayer = player;
            Random rand = new Random();

            List<GameMoveNode> moves = new List<GameMoveNode>();
            foreach (var pawn in player.Pawns)
            {
                List<Field> fields = pawn.GetAvailableFields();
                foreach (var field in fields)
                {
                    Field oldField = pawn.GetOcuppiedField();
                    moves.Add(new GameMoveNode(pawn, oldField, field));
                }
            }

            foreach (var move in moves)
            {
                move.Rating = GameMoveEvaluator.ComplexEvaluator(move, player.GetEndBase(), player);
            }
            GameMoveNode best = moves.OrderByDescending(x => x.Rating).FirstOrDefault();
            ControlledPlayer.MovePawn(best.Pawn, best.To.X, best.To.Y);

            return best;
        }
    }
}