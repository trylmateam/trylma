--drop database TRYLMA_DATABASE;
CREATE DATABASE TRYLMA_DATABASE
GO

USE TRYLMA_DATABASE
drop table Users;
CREATE TABLE Users
(
	login varchar(20) PRIMARY KEY,
	password varchar(50),
	id int NOT NULL identity(1,1),
	avatar varchar(100)/*varbinary(MAX)*/,
	ranking integer,
	opis varchar(250),
	playerType integer
);
insert into users values('HeuristicAI',' ','/Resources/Images/Avatars/dog2.jpg',300,'hejka, jestem AI!',1);
insert into users values('MinMaxAI',' ','/Resources/Images/Avatars/dog3.jpg',200,'hejka, jestem AI!',2);