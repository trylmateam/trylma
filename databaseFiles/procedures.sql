USE TRYLMA_DATABASE;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [getUser]
	-- Add the parameters for the stored procedure here
	@login varchar(50),
	@password varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Users
	where @login = Users.login AND
	@password = Users.password;
END
GO

CREATE PROCEDURE [getUserById]
	-- Add the parameters for the stored procedure here
	@id integer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Users
	where @id = Users.id;
END
GO

CREATE PROCEDURE [getUserByLogin]
	-- Add the parameters for the stored procedure here
	@login varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Users
	where @login = Users.login;
END
GO

CREATE PROCEDURE [addNewUser]
	-- Add the parameters for the stored procedure here
	@login varchar(50),
	@password varchar(50),
	@opis varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into Users values(@login,@password,'/Resources/Images/Avatars/default.jpg',100,@opis,0);
END
GO

CREATE PROCEDURE [changeRanking]
	-- Add the parameters for the stored procedure here
	@login varchar(50),
	@ranking integer
AS
BEGIN
	SET NOCOUNT ON;
	update Users set  Users.ranking=@ranking where Users.login=@login;
END
GO

CREATE PROCEDURE [setAvatar]
	@login varchar(50),
	@imgpath varchar(100)
AS
BEGIN
	update Users set  Users.avatar=@imgpath where Users.login=@login;
END
GO

CREATE PROCEDURE [getRankingList]
AS
BEGIN
	select * from Users order by Users.ranking desc;
END
GO